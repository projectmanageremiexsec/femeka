import { Competidor, AtletaAka, Semifinal1, Semifinal2, final, ResultSemi1, ResultSemi2, ResultFinal } from '../interfaces/atleta';
import { AtletasService } from '../services/atletas/atletas.service';

export default class AtletasCompe{

    public atletas:Competidor = {} as Competidor;
    public atletaAka:AtletaAka = {} as AtletaAka;
    public ateltasSemifinal1:Semifinal1 = {} as Semifinal1;
    public atletasSemifinal2:Semifinal2 = {} as Semifinal2;
    public atletasFinal:final = {} as final;

    public resultadosSemi1 = [];
    public resultadosSemi2 = [];
    public resultadosFinal = [];

    constructor() {
        this.obtenerAtleta();
        this.obtenerAtletaSemi1();
        this.obtenerAtletaSemi2();
        this.obtenerAtletaFinal();
        this.obtenerAtletasAka();
        this.obtenerAtletasSemi1();
        this.obtenerAtletasSemi2();
        this.obtenerAtletasFinal();
    }

    public obtenerAtleta(){
        this.atletas = JSON.parse(localStorage.getItem('competidores'));
        if ( this.atletas == null ){
          this.atletas = {
            competidoresAKA:{
              ronda1:[],
              ronda2:[],
              ronda3:[],
              ronda4:[]
            },
            competidoresAO:{
              ronda1:[],
              ronda2:[],
              ronda3:[],
              ronda4:[]
            }
          }
        }
        return this.atletas;
    }

    public obtenerAtletasAka(){
      this.atletaAka = JSON.parse(localStorage.getItem('registros'));
        if ( this.atletaAka == null ){
          this.atletaAka = {
            atletasAka:[],
            atletasAo:[]
          };
        }
        return this.atletaAka;
    }

    public obtenerAtletasSemi1(){
      this.ateltasSemifinal1 = JSON.parse(localStorage.getItem('semi1'));
        if ( this.ateltasSemifinal1 == null ){
          this.ateltasSemifinal1 = {
            atletas:[]
          };
        }
        return this.ateltasSemifinal1;
    }

    public obtenerAtletasSemi2(){
      this.atletasSemifinal2 = JSON.parse(localStorage.getItem('semi2'));
        if ( this.atletasSemifinal2 == null ){
          this.atletasSemifinal2 = {
            atletas:[]
          };
        }
        return this.atletasSemifinal2;
    }
    public obtenerAtletasFinal(){
      this.atletasFinal = JSON.parse(localStorage.getItem('final'));
        if ( this.atletasFinal == null ){
          this.atletasFinal = {
            atletas:[]
          };
        }
        return this.atletasFinal;
    }


    public agregarAtletas(atleta,ronda){
      switch (ronda) {
        case '1':
          this.atletas.competidoresAKA.ronda1.push(atleta)
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        case '2':
          this.atletas.competidoresAKA.ronda2.push(atleta)
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        case '3':
          this.atletas.competidoresAKA.ronda3.push(atleta)
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        case '4':
          this.atletas.competidoresAKA.ronda4.push(atleta)
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        default:
          break;
      }
    }

    public agregarAtletasAo(atleta,ronda){
      switch (ronda) {
        case '1':
          this.atletas.competidoresAO.ronda1.push( atleta );
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        case '2':
          this.atletas.competidoresAO.ronda2.push( atleta );
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        case '3':
          this.atletas.competidoresAO.ronda3.push( atleta );
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        case '4':
          this.atletas.competidoresAO.ronda4.push( atleta );
          localStorage.setItem('competidores', JSON.stringify(this.atletas))
          break;
        default:
          break;
      }
  }

  public agregarAtletasSemi1(atleta){
    this.resultadosSemi1.push(atleta)
    localStorage.setItem('resultSemi1', JSON.stringify(this.resultadosSemi1))
  }

  public agregarAtletasSemi2(atleta){
    this.resultadosSemi2.push(atleta)
    localStorage.setItem('resultSemi2', JSON.stringify(this.resultadosSemi2))
  }
  public agregarAtletasFinal(atleta){
    this.resultadosFinal.push(atleta)
    localStorage.setItem('resultFinal', JSON.stringify(this.resultadosFinal))
  }

    public async agregarAtletaEquipoAka(atleta){
      this.atletaAka.atletasAka.push(atleta);
      localStorage.setItem('registros', JSON.stringify(this.atletaAka))
    }

    public async agregarAtletaEquipoAo(atleta){
      this.atletaAka.atletasAo.push(atleta);
      localStorage.setItem('registros', JSON.stringify(this.atletaAka))
    }

    //Semifinales

    public async agregarAtletaEquipoAkaSemi1(atleta){
      this.ateltasSemifinal1.atletas.push(atleta);
      localStorage.setItem('semi1', JSON.stringify(this.ateltasSemifinal1))
    }

    public async agregarAtletaEquipoAoSemi2(atleta){
      this.atletasSemifinal2.atletas.push(atleta);
      localStorage.setItem('semi2', JSON.stringify(this.atletasSemifinal2))
    }

    //Final

    public async agregarAtletaFinal(atleta){
      this.atletasFinal.atletas.push(atleta);
      localStorage.setItem('final', JSON.stringify(this.atletasFinal))
    }

    public getDataAtleta() {
      let atletaJson = JSON.parse( localStorage.getItem('competidores'));
      if( atletaJson === null || atletaJson === undefined || atletaJson === "" ){
          return atletaJson =  {
            competidores:[]
          };
      }else {
          return atletaJson;
      }
    }

    public obtenerAtletaSemi1(){
      this.resultadosSemi1 = JSON.parse(localStorage.getItem('resultSemi1'));
      if ( this.resultadosSemi1 == null ){
        this.resultadosSemi1 = []
      }
      return this.resultadosSemi1;
  }
  public obtenerAtletaSemi2(){
    this.resultadosSemi2 = JSON.parse(localStorage.getItem('resultSemi2'));
    if ( this.resultadosSemi2 == null ){
      this.resultadosSemi2 = []
    }
    return this.resultadosSemi2;
}

public obtenerAtletaFinal(){
  this.resultadosFinal = JSON.parse(localStorage.getItem('resultFinal'));
  if ( this.resultadosFinal == null ){
    this.resultadosFinal = []
  }
  return this.resultadosFinal;
}

}
