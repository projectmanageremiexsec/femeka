import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  public usuario =""
  public pass =""
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    if ( this.usuario == "MIKE" && this.pass=="87654321")
    {
      this.router.navigate(['/dashboard'])
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'USUARIO O CONTRASEÑA INCORRECTA',
        // footer: '<a href="">Why do I have this issue?</a>'
      })
      
    }
  }

}
