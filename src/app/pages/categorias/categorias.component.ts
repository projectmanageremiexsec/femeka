import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosService } from '../../services/datos/datos.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

  public genero ='';
  public categorias=[];
  public categoria = '';
  public rango = '';
  public division = ''
  public categoriaTerminada=[];
  constructor(private _route:ActivatedRoute,
              private _router:Router,
              private _datos:DatosService) { }

  ngOnInit(): void {
    this.genero= this._route.snapshot.paramMap.get('genero');
    this.rango= this._route.snapshot.paramMap.get('rango');
    this.obtenerCatego();
  }

  obtenerCatego(){
    if(this.genero == 'Masculino'){
      this.categorias = this._datos.hombresNegras
    }else{
      this.categorias = this._datos.mujeresNegras
    }
  }

  navegar(categoria){
    this.categoria = categoria
    this.division = 'Ninguna';
    if(this.rango == 'Cinta negras'){
      this._router.navigate(['/dashboard/tablas/competencia/',this.genero,this.categoria,this.rango,this.division])
    }else{
      this._router.navigate(['/dashboard/divisiones/',this.genero,this.categoria,this.rango])
    }
  }

}
