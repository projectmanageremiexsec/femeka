import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';

@Component({
  selector: 'app-ganadores',
  templateUrl: './ganadores.component.html',
  styleUrls: ['./ganadores.component.scss']
})
export class GanadoresComponent implements OnInit {

  public categoria = ''
  public rango = ''
  public division = ''
  public genero = ''
  public primerLugar = [];
  public segundoLugar = [];
  public tercerLugar=[];
  constructor( public _atletas:AtletasService,
               public _route:ActivatedRoute) { }

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.rango = this._route.snapshot.paramMap.get('rango')
    this.division = this._route.snapshot.paramMap.get('division')
    this.genero = this._route.snapshot.paramMap.get('genero')
    this.obtenerGanadores();
  }

  obtenerGanadores(){
    this._atletas.obtenerTablaWinner().subscribe((resp:any)=>{
      this.setGanadores(resp['finalistas']);
      
    })
  }

  setGanadores(ganadores){ 
    ganadores.forEach(element => {
      if(element.rango == this.rango){
        if(element.idAtleta.valores[0].sexo == this.genero){
          if(element.idAtleta.valores[0].categoria == this.categoria){
            if(element.division == this.division){
              switch(element.lugar){
                case 'Primer Lugar':
                    this.primerLugar.push(element)
                  break;
                case 'Segundo Lugar':
                    this.segundoLugar.push(element)
                  break;
                case 'Tercer Lugar':
                    this.tercerLugar.push(element)
                    this.tercerLugar.sort(function(a,b){
                      if(a.puntajeTotal < b.puntajeTotal){
                        return 1;
                      }
                      if (a.puntajeTotal > b.puntajeTotal) {
                        return -1;
                      }
                      return 0;
                    })
                  break;
              }
            }
          }
        }
      }
    });
    console.log(this.primerLugar);
    console.log(this.segundoLugar);
    console.log(this.tercerLugar);
    
    
    
  }

}
