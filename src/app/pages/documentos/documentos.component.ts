import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatosService } from 'src/app/services/datos/datos.service';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss']
})
export class DocumentosComponent implements OnInit {
  
  public registro ={
    pool: "",
   division :"",
  vueltas :"",
   genero:"",
   grado:"",
categoria:""
  }

  public divisionesColor =[]
public categoriasAll =[]
  constructor(public _route: Router, public _datos:DatosService) { }

  ngOnInit(): void {
    this.diviciones()
  }
  enviarRegistro(){
    localStorage.setItem("registro",JSON.stringify(this.registro))
    this._route.navigate([ '/dashboard/numeros/pools'])
  }

  diviciones(){
    this.divisionesColor = this._datos.divisiones
  }

  categorias(){

    if(this.registro.grado =="Cinta Negra"){
      this.categoriasAll = this._datos.cintasNegras
      console.log("entro a negras");
      
    }
    else if (this.registro.grado =="Cinta Color"){
  this.categoriasAll = this._datos.cintasKyu
  console.log("entro a color");
  
    }

  }
}
