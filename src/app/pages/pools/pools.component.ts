import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-pools',
  templateUrl: './pools.component.html',
  styleUrls: ['./pools.component.scss']
})
export class PoolsComponent implements OnInit {

  public encabezado={
    categoria: "",
division: "",
genero: "",
grado: "",
pool: "",
vueltas: ""
  }

  public tabla ={
    atletaUno :"",
    atletaDos :"",
    atletaTres :"",
    atletaCuatro :"",
    atletaCinco :"",
    atletaSeis :"",
    atletaSiete :"",
    atletaOcho :"",
    atletaNueve :"",
    atletaDiez :"",
    atletaOnce :"",
    atletaDoce :"",
    estadoUno:"",
    estadoDos:"",
    estadoTres:"",
    estadoCuatro:"",
    estadoCinco:"",
    estadoSeis:"",
    estadoSiete:"",
    estadoOcho:"",
    estadoNueve:"",
    estadoDiez:"",
    estadoOnce:"",
    estadoDoce:"",
  }
  public contador 
  public letraPool=""
  constructor(public _route: Router) { }

  ngOnInit(): void {
 
    this.encabezado = JSON.parse(localStorage.getItem('registro')!)
    this.contador = this.encabezado.pool  
    
  }

  completado(){
    if(this.contador==0 || this.contador == "0"){
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Graficas Completas POOLS 0',
        showConfirmButton: false,
        timer: 4500
      })

      this._route.navigate([ '/dashboard/documentos'])

    }
  }

  imprimirPDF(){
    if(this.contador == -1 || this.contador == "-1" ){
       this.contador =0
    }
   


    if(this.contador == '1' || this.contador == 3 || this.contador == 5 || this.contador == 7 || this.contador == 9 || this.contador == 11){
       this.letraPool='A'
       console.log("entro a pool A");
       
    }
    else if (this.contador == '2' || this.contador == 4 || this.contador == 6 || this.contador == 8 || this.contador == 10 || this.contador == 12){
      this.letraPool='B'
      console.log("entro a pool B");
    }

    const doc : any = new jsPDF({ orientation: 'landscape'});
    doc.setTextColor(100);
  doc.setFont('courier')
  doc.text('GRAFICA DE COMPETIDORES  MODALIDAD kATA', 90, 10);
  doc.text('#POOL : ' + this.letraPool, 245, 10);
  doc.text( "CATEGORIA: "+ this.encabezado.categoria, 10, 30);
  doc.text( "GRADO: "+ this.encabezado.grado, 200, 30);
  doc.text( "DIVISION: "+ this.encabezado.division, 10, 40);
  doc.text( "SEXO: "+ this.encabezado.genero, 200, 40);
  var img = new Image()
  img.src = 'assets/logoFemeka.png'
  doc.addImage(img, 'png', 120, 180, 30, 30)

  if(this.contador == '1' || this.contador == 3 || this.contador == 5 || this.contador == 7 || this.contador == 9 || this.contador == 11){


    doc.autoTable({
      theme:'grid',
       styles: { halign: 'center',font :'courier',fontSize:18 , fillColor:[255,0,0]},
      columnStyles: {0:{  halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     1:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] }, 
                     2:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     3:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     4:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     5:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     6:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     7:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] }},
      head:[["COMPETIDOR","ESTADO","R1"," R2","R3","R4","R5","R6"]],
     
      body :[
        [this.tabla.atletaUno,this.tabla.estadoUno,"","","","","",""],
        [this.tabla.atletaDos,this.tabla.estadoDos,"","","","","",""],
        [this.tabla.atletaTres,this.tabla.estadoTres,"","","","","",""],
        [this.tabla.atletaCuatro,this.tabla.estadoCuatro,"","","","","",""],
        [this.tabla.atletaCinco,this.tabla.estadoCinco,"","","","","",""],
        [this.tabla.atletaSeis,this.tabla.estadoSeis,"","","","","",""],
        [this.tabla.atletaSiete,this.tabla.estadoSiete,"","","","","",""],
        [this.tabla.atletaOcho,this.tabla.estadoOcho,"","","","","",""],
        [this.tabla.atletaNueve,this.tabla.estadoNueve,"","","","","",""],
        [this.tabla.atletaDiez,this.tabla.estadoDiez,"","","","","",""],
        [this.tabla.atletaOnce,this.tabla.estadoOnce,"","","","","",""],
        [this.tabla.atletaDoce,this.tabla.estadoDoce,"","","","","",""],
  
      ],
      startY:50
    });
    

  }
  else if (this.contador == '2' || this.contador == 4 || this.contador == 6 || this.contador == 8 || this.contador == 10 || this.contador == 12){

    doc.autoTable({
      theme:'grid',
       styles: { halign: 'center',font :'courier',fontSize:18, fillColor:[100,149,237] },
      columnStyles: {0:{  halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     1:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] }, 
                     2:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     3:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     4:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     5:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     6:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     7:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] }},
      head:[["COMPETIDOR","ESTADO","R1"," R2","R3","R4","R5","R6"]],
     
      body :[
        [this.tabla.atletaUno,this.tabla.estadoUno,"","","","","",""],
        [this.tabla.atletaDos,this.tabla.estadoDos,"","","","","",""],
        [this.tabla.atletaTres,this.tabla.estadoTres,"","","","","",""],
        [this.tabla.atletaCuatro,this.tabla.estadoCuatro,"","","","","",""],
        [this.tabla.atletaCinco,this.tabla.estadoCinco,"","","","","",""],
        [this.tabla.atletaSeis,this.tabla.estadoSeis,"","","","","",""],
        [this.tabla.atletaSiete,this.tabla.estadoSiete,"","","","","",""],
        [this.tabla.atletaOcho,this.tabla.estadoOcho,"","","","","",""],
        [this.tabla.atletaNueve,this.tabla.estadoNueve,"","","","","",""],
        [this.tabla.atletaDiez,this.tabla.estadoDiez,"","","","","",""],
        [this.tabla.atletaOnce,this.tabla.estadoOnce,"","","","","",""],
        [this.tabla.atletaDoce,this.tabla.estadoDoce,"","","","","",""],
  
      ],
      startY:50
    });
    

  }
  
  
  doc.text("__________________________",10,195)
  doc.text(" NOMBRE Y FIRMA",25,200)

  //lugares
  doc.text("__________________________",175,175)
  doc.text("1ER.LUGAR :",180,185)
  doc.text("2DO.LUGAR :",180,190)
  doc.text("3ER.LUGAR :",180,195)
  doc.text("3ER.LUGAR :",180,200)
  doc.text("__________________________",175,205)

 
   // Open PDF document in new tab
     doc.output('dataurlnewwindow')
  this.tabla={atletaUno :"",
  atletaDos :"",
  atletaTres :"",
  atletaCuatro :"",
  atletaCinco :"",
  atletaSeis :"",
  atletaSiete :"",
  atletaOcho :"",
  atletaNueve :"",
  atletaDiez :"",
  atletaOnce :"",
  atletaDoce :"",
  estadoUno:"",
  estadoDos:"",
  estadoTres:"",
  estadoCuatro:"",
  estadoCinco:"",
  estadoSeis:"",
  estadoSiete:"",
  estadoOcho:"",
  estadoNueve:"",
  estadoDiez:"",
  estadoOnce:"",
  estadoDoce:"",}
  this.contador = this.contador -1
 this.completado();
  }
}
