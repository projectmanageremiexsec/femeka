import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroCompeComponent } from './registro-compe.component';

describe('RegistroCompeComponent', () => {
  let component: RegistroCompeComponent;
  let fixture: ComponentFixture<RegistroCompeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroCompeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroCompeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
