import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';

@Component({
  selector: 'app-registro-compe',
  templateUrl: './registro-compe.component.html',
  styleUrls: ['./registro-compe.component.scss']
})
export class RegistroCompeComponent implements OnInit {

  public atleta=[];
  public equipoAka=[];
  public equipoAo=[];
  public selectDevice='';
  public pool = ""
  public division =""
  public vueltas =""
  public genero=""

  public registro ={
    pool: "",
   division :"",
  vueltas :"",
   genero:"",
   grado:"",
categoria:""
  }
  constructor(public _atletas:AtletasService,
              public _route: Router) { }

  ngOnInit(): void {
    this.obtenerAtletas();
  }

  obtenerAtletas(){
    this._atletas.obtenerAtletas().subscribe((resp:any) => {
      this.setAtleta(resp.respuesta);
    })
  }

  setAtleta(atletas){
    atletas.forEach(element => {
      this.atleta.push(element)
    });
    this.equipos(this.atleta)
  }

  equipos(array){
    array.sort(function() { return Math.random() - 0.5 });
    console.log(array)
    var index = array.length;
    this.equipoAka = array.slice(0, index/2);
    this.equipoAo = array.slice(index/2,index);
    console.log(this.equipoAka);
    console.log(this.equipoAo);
  }

  equipoaka(event){
    this._route.navigate(['/dashboard/competidor/aka/',event,this.selectDevice])
  }

  equipoao(event){
    this._route.navigate(['/dashboard/competidor/ao/',event,this.selectDevice])
  }

  enviarRegistro(){
    localStorage.setItem("registro",JSON.stringify(this.registro))
    this._route.navigate([ '/dashboard/numeros/pools'])
  }
}
