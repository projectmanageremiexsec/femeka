import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaRangoComponent } from './categoria-rango.component';

describe('CategoriaRangoComponent', () => {
  let component: CategoriaRangoComponent;
  let fixture: ComponentFixture<CategoriaRangoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriaRangoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriaRangoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
