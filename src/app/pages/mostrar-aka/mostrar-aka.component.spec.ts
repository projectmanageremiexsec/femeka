import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarAkaComponent } from './mostrar-aka.component';

describe('MostrarAkaComponent', () => {
  let component: MostrarAkaComponent;
  let fixture: ComponentFixture<MostrarAkaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarAkaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarAkaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
