import { Component, OnInit } from '@angular/core';
import { Competidor } from 'src/app/interfaces/atleta';

@Component({
  selector: 'app-mostrar-aka',
  templateUrl: './mostrar-aka.component.html',
  styleUrls: ['./mostrar-aka.component.scss']
})
export class MostrarAkaComponent implements OnInit {

  public competidores:Competidor = {} as Competidor;
  public cali:any[] = []

  constructor() { }

  ngOnInit(): void {
    this.mostrarAtleta(JSON.parse(localStorage.getItem('mostrarCompe')).atleta);
  }

  mostrarAtleta(atleta){
    this.competidores = JSON.parse(localStorage.getItem('competidores')).competidoresAKA
    for (const key in this.competidores) {
      switch (key) {
        case 'ronda1':
          let ronda1 = this.competidores[key].find(element => element.atleta._id == atleta._id )
          if(ronda1 != undefined){
            this.cali.push(ronda1);
          }
          break;
        case 'ronda2':
          let ronda2 = this.competidores[key].find(element => element.atleta._id == atleta._id )
          if(ronda2 != undefined){
            this.cali.push(ronda2);
          }
          break;
        case 'ronda3':
          let ronda3 = this.competidores[key].find(element => element.atleta._id == atleta._id )
          if(ronda3 != undefined){
            this.cali.push(ronda3);
          }
          break;
        case 'ronda4':
          let ronda4 = this.competidores[key].find(element => element.atleta._id == atleta._id )
          if(ronda4 != undefined){
            this.cali.push(ronda4);
          }
          break;

        default:
          break;
      }
    }
    console.log(this.cali);

  }

}
