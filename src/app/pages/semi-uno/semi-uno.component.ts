import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';
import { DatosService } from 'src/app/services/datos/datos.service';

@Component({
  selector: 'app-semi-uno',
  templateUrl: './semi-uno.component.html',
  styleUrls: ['./semi-uno.component.scss']
})
export class SemiUnoComponent implements OnInit {


  public activado = true;
  public datosCompetidorRojo ={
    equipo: "",
    idAtleta : { valores:[],_id:""},
    kata: "",
    nivelAtletico: [],
    nivelTecnico: [],
    puntajeTotal:0,
    ronda: "",
    tatami: "",
    _id:""

  }
  public datosCompetidorAzul ={
    equipo: "",
    idAtleta : { valores:[],_id:""},
    kata: "",
    nivelAtletico: [],
    nivelTecnico: [],
    puntajeTotal:0,
    ronda: "",
    tatami: "",
    _id:""

  }

  public selectDevice='';
  public kata = [];
  public semiTatami =""
  public tatami = [];
  public  division ="Ninguna"

  public resultadosSemiUno =[];
  public resultRojo = true;
  public resultAzul = true;

  public rango ='';
  public divi="";

  constructor(public _atletas:AtletasService,
              public _router: Router,
              public routes: ActivatedRoute,
              public _datos:DatosService) { }

  ngOnInit(): void {
    this.rango = this.routes.snapshot.paramMap.get('rango');
    this.divi = this.routes.snapshot.paramMap.get('division');
    this.katas();
  }

  katas(){
    this.kata = this._datos.katas
  }

  setSemi(result){
    this.resultadosSemiUno = result

    result.forEach(element => {
      if(element.idAtleta._id == this.datosCompetidorAzul.idAtleta._id){
          this.resultAzul = false;
        }
        else if (element.idAtleta._id == this.datosCompetidorRojo.idAtleta._id){
          this.resultRojo =false;
        }
    });

    this.resultadosSemiUno.sort(function(a,b){
      if(a.puntajeTotal < b.puntajeTotal){
        return 1;
      }
      if (a.puntajeTotal > b.puntajeTotal) {
        return -1;
      }
      return 0;
    })
    console.log(this.resultadosSemiUno);
  }

  equipoaka(event,ronda){
    this._router.navigate(['/dashboard/competidor/aka/',event,this.selectDevice,this.semiTatami,ronda,this.datosCompetidorRojo.idAtleta.valores[0].colorCinta , this.division])

  }
  equipoao(event,ronda){
    this._router.navigate(['/dashboard/competidor/ao/',event,this.selectDevice,this.semiTatami,ronda,this.datosCompetidorAzul.idAtleta.valores[0].colorCinta , this.division])
  }

}
