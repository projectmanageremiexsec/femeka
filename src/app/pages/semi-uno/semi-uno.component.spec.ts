import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SemiUnoComponent } from './semi-uno.component';

describe('SemiUnoComponent', () => {
  let component: SemiUnoComponent;
  let fixture: ComponentFixture<SemiUnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SemiUnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SemiUnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
