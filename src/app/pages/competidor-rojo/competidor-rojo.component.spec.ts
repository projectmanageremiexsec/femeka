import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetidorRojoComponent } from './competidor-rojo.component';

describe('CompetidorRojoComponent', () => {
  let component: CompetidorRojoComponent;
  let fixture: ComponentFixture<CompetidorRojoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompetidorRojoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetidorRojoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
