import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasRangoKumiteComponent } from './categorias-rango-kumite.component';

describe('CategoriasRangoKumiteComponent', () => {
  let component: CategoriasRangoKumiteComponent;
  let fixture: ComponentFixture<CategoriasRangoKumiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriasRangoKumiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasRangoKumiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
