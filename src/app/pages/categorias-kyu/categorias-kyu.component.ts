import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosService } from '../../services/datos/datos.service';

@Component({
  selector: 'app-categorias-kyu',
  templateUrl: './categorias-kyu.component.html',
  styleUrls: ['./categorias-kyu.component.scss']
})
export class CategoriasKyuComponent implements OnInit {
  public genero ='';
  public categorias=[];
  public categoria = '';
  public rango = '';
  public division = '';
  public categoriaTerminada = [];
  constructor(private _route:ActivatedRoute,
              private _router:Router,
              private _datos:DatosService) {}

  ngOnInit(): void {
    this.genero= this._route.snapshot.paramMap.get('genero');
    this.rango= this._route.snapshot.paramMap.get('rango');
    this.obtenerCatego();
  }

  obtenerCatego(){
    if(this.genero == 'Masculino'){
      this.categorias = this._datos.hombresKyu
    }else{
      this.categorias = this._datos.mujeresKyu
    }
  }

  navegar(categoria){
    this.categoria = categoria
    this._router.navigate(['/dashboard/divisiones/',this.genero,this.categoria,this.rango])
  }
}
