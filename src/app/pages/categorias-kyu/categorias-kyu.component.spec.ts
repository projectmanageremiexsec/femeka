import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasKyuComponent } from './categorias-kyu.component';

describe('CategoriasKyuComponent', () => {
  let component: CategoriasKyuComponent;
  let fixture: ComponentFixture<CategoriasKyuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriasKyuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasKyuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
