import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasGraficasComponent } from './categorias-graficas.component';

describe('CategoriasGraficasComponent', () => {
  let component: CategoriasGraficasComponent;
  let fixture: ComponentFixture<CategoriasGraficasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriasGraficasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasGraficasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
