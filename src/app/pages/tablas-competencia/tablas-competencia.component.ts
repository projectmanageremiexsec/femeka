import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tablas-competencia',
  templateUrl: './tablas-competencia.component.html',
  styleUrls: ['./tablas-competencia.component.scss']
})
export class TablasCompetenciaComponent implements OnInit {

  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  public categoria = '';
  public genero='';
  public atletas = [];
  public teamRojo = [];
  public teamAzul = [];
  public kata = [];
  public selectDevice='';
  public categoria2=false;
  public categoria4=false;
  public categoria10=false;
  public categoria24=false;
  public categoria48=false;
  public categoria96=false;
  public rango = '';
  public division = '';
  constructor(public _route:ActivatedRoute,
              public _atletas:AtletasService,
              public _router: Router,
              private modal: NgbModal) { }

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero =  this._route.snapshot.paramMap.get('genero')
    this.rango =  this._route.snapshot.paramMap.get('rango')
    this.division = this._route.snapshot.paramMap.get('division')
    this.obtenerAtletas();
  }

  obtenerAtletas(){
    this.seleccionarCategoria(10);
  }

  /* numeroAtletas(form:NgForm){
    localStorage.setItem('numeroAtletas', form.value.atletas)
    this.seleccionarCategoria(form.value.atletas);
    this.modal.dismissAll();
  } */

  async abrirModal(){
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  seleccionarCategoria(numero){
    let categoria='';
    if(numero == 2 || numero == 3){
      categoria='categoria2'
    }
    if(numero == 4){
      categoria='categoria4'
    }
    if(numero >= 5 && numero <= 10){
      categoria = 'categoria10';
    }else if(numero >= 11 && numero <= 24){
      categoria = 'categoria24';
    }else if(numero >= 25 && numero <= 48){
      categoria = 'categoria48'
    }
    else if(numero >= 49 && numero <= 96){
      categoria = 'categoria96'
    }
    switch (categoria) {
      case 'categoria2':
        this.categoria24 = false;
        this.categoria48 = false;
        this.categoria10 = false;
        this.categoria2=true;
        this.categoria4 = false;
        break;
      case 'categoria4':
        this.categoria24 = false;
        this.categoria48 = false;
        this.categoria10 = false;
        this.categoria2=false;
        this.categoria4 = true;
        break;
      case 'categoria10':
        this.categoria24 = false;
        this.categoria48 = false;
        this.categoria10 = true;
        this.categoria2=false;
        this.categoria4 = false;
        break;
      case 'categoria24':
        this.categoria10 = false;
        this.categoria48 = false;
        this.categoria24 = true;
        this.categoria2=false;
        this.categoria4 = false;
        break;
      case 'categoria48':
        this.categoria10 = false;
        this.categoria24 = false;
        this.categoria48 = true;
        this.categoria2=false;
        this.categoria4 = false;
        break;
      case 'categoria96':
        this.categoria10 = false;
        this.categoria24 = false;
        this.categoria48 = false;
        this.categoria96= true;
        this.categoria2=false;
        this.categoria4 = false;
        break;

      default:
        Swal.fire({
          title: 'No hay ningun atleta registrado',
          confirmButtonText: `Ok`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            this._router.navigate(['/dashboard']);
          }
        })
        break;
    }
  }

  /* setAtletas(atletas){
    atletas.forEach(element => {
      if (element.valores[0].modalidad == 'Kata') {
        if(element.valores[0].sexo == this.genero){
          if(element.valores[0].categoria === this.categoria){
            this.atletas.push(element)
          }
        }
      }
    });
    this.equipos(this.atletas);
  }

  equipos(atletas){
    if (atletas.length == 2 || atletas.length == 3) {
      atletas.forEach(element => {
        this.teamRojo.push(element)
      });
      this.teamRojo.sort(function() { return Math.random() - 0.5 });
      console.log(this.teamRojo)
    } else {
      atletas.sort(function() { return Math.random() - 0.5 });
      console.log(atletas)
      var index = atletas.length;
      this.teamRojo = atletas.slice(0, index/2);
      this.teamAzul = atletas.slice(index/2,index);
      console.log(this.teamRojo);
      console.log(this.teamAzul);
    }
  }

  katas(){
    this._atletas.obtenerKatas().subscribe((resp:any) =>{
      if (resp.ok) {
        this.setKatas(resp['katas']);
      }
    })
  }

  setKatas(katas){
    this.kata = katas
  }

  equipoaka(event){
    this._router.navigate(['/dashboard/competidor/aka/',event,this.selectDevice])
  }

  equipoao(event){
    this._router.navigate(['/dashboard/competidor/ao/',event,this.selectDevice])
  } */

}
