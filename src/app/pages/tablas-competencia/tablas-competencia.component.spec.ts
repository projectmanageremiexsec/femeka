import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablasCompetenciaComponent } from './tablas-competencia.component';

describe('TablasCompetenciaComponent', () => {
  let component: TablasCompetenciaComponent;
  let fixture: ComponentFixture<TablasCompetenciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablasCompetenciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablasCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
