import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';
import AtletasCompe from '../../atletas/nuevoAtleta'

@Component({
  selector: 'app-competidor',
  templateUrl: './competidor.component.html',
  styleUrls: ['./competidor.component.scss']
})
export class CompetidorComponent implements OnInit {

  public id;
  public atleta={
    apellidoMaterno: "",
    apellidoPaterno: "",
    estado: "",
    nombre: "",
    genero:"",
    categoria:""
  };
  public tatami = '';
  public kataCompetidor;
  public juez1Tecnico:number=0;
  public juez2Tecnico:number=0;
  public juez3Tecnico:number=0;
  public juez4Tecnico:number=0;
  public juez5Tecnico:number=0;
  public juez1Atletico:number=0;
  public juez2Atletico:number=0;
  public juez3Atletico:number=0;
  public juez4Atletico:number=0;
  public juez5Atletico:number=0;
  public calificacionTecnico:number=0;
  public calificacionMaxTecnico:number = 0;
  public calificacionMinTecnico:number = 0;
  public calificacionAtletico:number=0;
  public calificacionMaxAtletico:number = 0;
  public calificacionMinAtletico:number = 0;
  public finalTecnico:number=0;
  public finalAtletico:number=0;
  public final:number = 0;
  public datosAtletaupdate:any;
  public calificacionesAtleticas={
    juez1:0,
    juez2:0,
    juez3:0,
    juez4:0,
    juez5:0,
    calificacionAtletico:0,
    calificacionMaxAtletico:0,
    calificacionMinAtletico:0,
    finalAtletico:0
  };
  public calificacionesTecnicas={
    juez1:0,
    juez2:0,
    juez3:0,
    juez4:0,
    juez5:0,
    calificacionTecnico:0,
    calificacionMaxTecnico:0,
    calificacionMinTecnico:0,
    finalTecnico:0
  };
  public ronda='';
  public rango = '';
  public division = '';
  public genero = '';
  public categoria = '';
  public idCate = '';

  constructor(private _route: ActivatedRoute,
              private _atletas:AtletasService,
              private _router: Router) { }

  ngOnInit(): void {
    this.kataCompetidor = this._route.snapshot.paramMap.get('kata');
    this.tatami = this._route.snapshot.paramMap.get('tatami');
    this.ronda = this._route.snapshot.paramMap.get('ronda');
    this.rango = this._route.snapshot.paramMap.get('rango');
    this.division = this._route.snapshot.paramMap.get('division');
    this.categoria = this._route.snapshot.paramMap.get('categoria');
    this.genero = this._route.snapshot.paramMap.get('genero');
    this.idCate = this._route.snapshot.paramMap.get('id');
    this.obtenerAtleta();
    /* this.actualizarKata(this.id); */
  }

  obtenerAtleta(){
    this.atleta = JSON.parse(localStorage.getItem('atleta'))
  }

  competidorAKA(){
    this.calificacionTecnico = this.juez1Tecnico + this.juez2Tecnico + this.juez3Tecnico + this.juez4Tecnico + this.juez5Tecnico
    this.numeroMayor('aka');
  }

  competidorAO(){
    this.calificacionAtletico = this.juez1Atletico + this.juez2Atletico + this.juez3Atletico + this.juez4Atletico + this.juez5Atletico
    this.numeroMayor('ao');
  }

  numeroMayor(tipo){
    if(tipo == 'aka'){
      this.calificacionMaxTecnico = 0;
      this.calificacionMaxTecnico = Math.max(this.juez1Tecnico, this.juez2Tecnico, this.juez3Tecnico , this.juez4Tecnico , this.juez5Tecnico);
      this.calificacionTecnico = parseFloat((this.calificacionTecnico - this.calificacionMaxTecnico).toFixed(2))
      this.numeroMenor('aka');
    }else{
      this.calificacionMaxAtletico = 0;
      this.calificacionMaxAtletico = Math.max(this.juez1Atletico, this.juez2Atletico, this.juez3Atletico , this.juez4Atletico , this.juez5Atletico);
      this.calificacionAtletico = parseFloat((this.calificacionAtletico - this.calificacionMaxAtletico).toFixed(2))
      this.numeroMenor('ao');
    }
  }

  numeroMenor(tipo){
    if(tipo == 'aka'){
      this.calificacionMinTecnico = 0;
      this.calificacionMinTecnico = Math.min(this.juez1Tecnico, this.juez2Tecnico, this.juez3Tecnico , this.juez4Tecnico , this.juez5Tecnico);
      this.calificacionTecnico = parseFloat((this.calificacionTecnico - this.calificacionMinTecnico).toFixed(2))
      this.calificacionFinal('aka');
    }else{
      this.calificacionMinAtletico = 0;
      this.calificacionMinAtletico = Math.min(this.juez1Atletico, this.juez2Atletico, this.juez3Atletico , this.juez4Atletico , this.juez5Atletico);
      this.calificacionAtletico = parseFloat((this.calificacionAtletico - this.calificacionMinAtletico).toFixed(2))
      this.calificacionFinal('ao');
    }
  }

  calificacionFinal(tipo){
    if(tipo == 'aka'){
      this.finalTecnico = parseFloat((this.calificacionTecnico * .70).toFixed(2));
      this.final = parseFloat((this.finalAtletico + this.finalTecnico).toFixed(2));
    }else{
      this.finalAtletico = parseFloat((this.calificacionAtletico * .30).toFixed(2));
      this.final = parseFloat((this.finalAtletico + this.finalTecnico).toFixed(2));
    }
  }

  generarDatosAtleticos(){
    this.calificacionesAtleticas.juez1=this.juez1Atletico
    this.calificacionesAtleticas.juez2=this.juez2Atletico
    this.calificacionesAtleticas.juez3=this.juez3Atletico
    this.calificacionesAtleticas.juez4=this.juez4Atletico
    this.calificacionesAtleticas.juez5=this.juez5Atletico
    this.calificacionesAtleticas.calificacionAtletico=this.calificacionAtletico
    this.calificacionesAtleticas.calificacionMaxAtletico=this.calificacionMaxAtletico
    this.calificacionesAtleticas.calificacionMinAtletico=this.calificacionMinAtletico
    this.calificacionesAtleticas.finalAtletico=this.finalAtletico
  }

  generarDatosTecnicos(){
    this.calificacionesTecnicas.juez1=this.juez1Tecnico
    this.calificacionesTecnicas.juez2=this.juez2Tecnico
    this.calificacionesTecnicas.juez3=this.juez3Tecnico
    this.calificacionesTecnicas.juez4=this.juez4Tecnico
    this.calificacionesTecnicas.juez5=this.juez5Tecnico
    this.calificacionesTecnicas.calificacionTecnico=this.calificacionTecnico
    this.calificacionesTecnicas.calificacionMaxTecnico=this.calificacionMaxTecnico
    this.calificacionesTecnicas.calificacionMinTecnico=this.calificacionMinTecnico
    this.calificacionesTecnicas.finalTecnico=this.finalTecnico
  }

  enviarDatos(){
    this.generarDatosAtleticos();
    this.generarDatosTecnicos();
    let atle = {
      atleta:this.atleta,
      kata:this.kataCompetidor,
      calificacionesAtleticas:this.calificacionesAtleticas,
      calificacionesTecnicas:this.calificacionesTecnicas,
      calificacionFinal:this.final
    }
    let compe = new AtletasCompe();
    compe.agregarAtletasAo(atle,this.ronda);
    let data = {
      resultados:JSON.parse(localStorage.getItem('competidores'))
    }
    this._atletas.actualizarCompetencia(this.idCate,data).subscribe((resp:any)=>{
      console.log(resp);
    })
    this._router.navigate(['/dashboard/tablas/competencia',this.genero, this.categoria, this.rango, this.division])
  }



}
