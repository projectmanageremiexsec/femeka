import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../components/components.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { RegistroCompeComponent } from './registro-compe/registro-compe.component';
import { SharedModule } from '../shared/shared.module';
import { CompetidorComponent } from './competidor/competidor.component';
import { GraficasKumiteComponent } from './graficas-kumite/graficas-kumite.component';
import { DocumentosComponent } from './documentos/documentos.component';
import { CompetidorRojoComponent } from './competidor-rojo/competidor-rojo.component';
import { TablasRankingComponent } from './tablas-ranking/tablas-ranking.component';
import { DescargarDocumentComponent } from './descargar-document/descargar-document.component';
import { CategoriaGeneroComponent } from './categoria-genero/categoria-genero.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { TablasCompetenciaComponent } from './tablas-competencia/tablas-competencia.component';
import { CategoriaRangoComponent } from './categoria-rango/categoria-rango.component';
import { DivisionesComponent } from './divisiones/divisiones.component';
import { CategoriasKyuComponent } from './categorias-kyu/categorias-kyu.component';
import { SemiUnoComponent } from './semi-uno/semi-uno.component';
import { SemiDosComponent } from './semi-dos/semi-dos.component';
import { FinalComponent } from './final/final.component';
import { CategoriasGraficasComponent } from './categorias-graficas/categorias-graficas.component';
import { CategoriasRangoKumiteComponent } from './categorias-rango-kumite/categorias-rango-kumite.component';
import { CategoriasGeneroKumiteComponent } from './categorias-genero-kumite/categorias-genero-kumite.component';
import { CategoriasGraficasKyuComponent } from './categorias-graficas-kyu/categorias-graficas-kyu.component';
import { DivisionesKumiteComponent } from './divisiones-kumite/divisiones-kumite.component';
import { GanadoresComponent } from './ganadores/ganadores.component';
import { PoolsComponent } from './pools/pools.component';
import { MostrarAkaComponent } from './mostrar-aka/mostrar-aka.component';
import { MostrarAoComponent } from './mostrar-ao/mostrar-ao.component';
import { GenerarTablasComponent } from './generar-tablas/generar-tablas.component';




@NgModule({
  declarations: [
    DashboardComponent,
    RegistroCompeComponent,
    PagesComponent,
    CompetidorComponent,
    GraficasKumiteComponent,
    DocumentosComponent,
    CompetidorRojoComponent,
    TablasRankingComponent,
    DescargarDocumentComponent,
    CategoriaGeneroComponent,
    CategoriasComponent,
    TablasCompetenciaComponent,
    CategoriaRangoComponent,
    DivisionesComponent,
    CategoriasKyuComponent,
    SemiUnoComponent,
    SemiDosComponent,
    FinalComponent,
    CategoriasGraficasComponent,
    CategoriasRangoKumiteComponent,
    CategoriasGeneroKumiteComponent,
    CategoriasGraficasKyuComponent,
    DivisionesKumiteComponent,
    GanadoresComponent,
    PoolsComponent,
    MostrarAkaComponent,
    MostrarAoComponent,
    GenerarTablasComponent,
  ],
  exports: [
    DashboardComponent,
    RegistroCompeComponent,
    PagesComponent,
    CompetidorComponent,
    GraficasKumiteComponent,
    DocumentosComponent,
    CompetidorRojoComponent,
    TablasRankingComponent,
    DescargarDocumentComponent,
    CategoriaGeneroComponent,
    CategoriasComponent,
    TablasCompetenciaComponent,
    CategoriaRangoComponent,
    DivisionesComponent,
    CategoriasKyuComponent,
    SemiUnoComponent,
    SemiDosComponent,
    FinalComponent,
    CategoriasGraficasComponent,
    CategoriasRangoKumiteComponent,
    CategoriasGeneroKumiteComponent,
    CategoriasGraficasKyuComponent,
    DivisionesKumiteComponent,
    GanadoresComponent,
    PoolsComponent,
    MostrarAkaComponent,
    MostrarAoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule,
    ComponentsModule
  ]
})
export class PagesModule { }
