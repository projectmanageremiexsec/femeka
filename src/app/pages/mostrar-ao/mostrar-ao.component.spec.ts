import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarAoComponent } from './mostrar-ao.component';

describe('MostrarAoComponent', () => {
  let component: MostrarAoComponent;
  let fixture: ComponentFixture<MostrarAoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarAoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarAoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
