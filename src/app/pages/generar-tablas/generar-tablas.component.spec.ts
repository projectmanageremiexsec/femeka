import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarTablasComponent } from './generar-tablas.component';

describe('GenerarTablasComponent', () => {
  let component: GenerarTablasComponent;
  let fixture: ComponentFixture<GenerarTablasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerarTablasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarTablasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
