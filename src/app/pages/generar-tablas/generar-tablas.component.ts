import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { generar } from '../../functions/generarPelea';

@Component({
  selector: 'app-generar-tablas',
  templateUrl: './generar-tablas.component.html',
  styleUrls: ['./generar-tablas.component.scss']
})
export class GenerarTablasComponent implements OnInit {

  public categoria = '';
  public competidores = 0;
  public atletas:any [] = [];
  public nombreAtleta = '';
  public estadoAtleta = '';
  public atletasRojos:any[] = [];
  public atletasAzules:any [] = [];
  public atletaRojo:any[] = []
  public atletaAzul:any[] = []
  public contador
  public letraPool=""
  public encabezado={
    categoria: "",
    division: "",
    genero: "",
    grado: "",
    pool: "",
    vueltas: ""
  }
  public peleasRojos:any[] = []
  public peleasAzules:any[] = []

  constructor() { }

  ngOnInit(): void {
  }

  reset(){
    this.atletas = []
    this.atletasAzules = []
    this.atletasRojos = []
    this.peleasRojos = []
    this.peleasAzules = []
  }

  agregarAtleta(form:NgForm){
    this.atletas.push(form.form.value)
    this.nombreAtleta = ''
    this.estadoAtleta = ''
  }

  sotear(){
    let index = this.atletas.length;
    if (this.atletaRojo.length == 0 && this.atletaAzul.length == 0 ) {
      this.atletas.sort(()=> Math.random() - 0.5);
      this.atletasRojos = this.atletas.slice(index/2,index)
      this.atletasAzules = this.atletas.slice(0, index/2);
    } else {
      this.atletas.sort(()=> Math.random() - 0.5);
      this.atletasRojos = this.atletas.slice(index/2,index)
      this.atletasAzules = this.atletas.slice(0, index/2);
      this.atletasRojos.push(this.atletaRojo[0])
      this.atletasAzules.push(this.atletaAzul[0])
      this.atletasRojos.reverse()
      this.atletasAzules.reverse()
    }
  }

  generarPelea(){
    this.peleasRojos = generar(this.atletasRojos);
    this.peleasAzules = generar(this.atletasAzules);
  }

  quitarAtletas(){
    this.atletaRojo.push(this.atletas[1]);
    this.atletaAzul.push(this.atletas[0]);
    this.atletas.splice(0,2)
  }

  imprimirPDF(){
    let contador =1
    let contador2 =1
    const data : any =[]
    const data2 : any =[]
    const doc : any = new jsPDF({ orientation: 'landscape'});
    doc.setTextColor(100);
    doc.setFont('courier')
    doc.text('GRAFICA', 90, 10);
    doc.text( "CATEGORIA: "+ this.categoria, 10, 20);
    doc.text( "NUMERO DE COMPETIDORES: "+ this.competidores, 200, 20);
    this.atletasRojos.forEach((element:any) => {
      var x =[
        contador,
        element.nombre,
        element.estado
    ]
    contador = contador + 1;
      data.push(x)
    });
    doc.autoTable({
      theme:'grid',
       styles: { halign: 'center',font :'courier',fontSize:18 , fillColor:[255,0,0]},
      columnStyles: {0:{  halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     1:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     2:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     3:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     4:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     5:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     6:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     7:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] }},
      head:[["#","COMPETIDOR","ESTADO"]],

      body :data,
      startY:30
    });

    this.atletasAzules.forEach((element:any) => {
      var x =[
        contador2,
        element.nombre,
        element.estado
    ]
    contador2 = contador2 + 1;
      data2.push(x)
    });

    doc.autoTable({
      theme:'grid',
       styles: { halign: 'center',font :'courier',fontSize:18, fillColor:[100,149,237] },
      columnStyles: {0:{  halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     1:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     2:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     3:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     4:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     5:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     6:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] },
                     7:{halign: 'center',font :'courier',fontSize:9 ,fillColor: [248,248,255] }},
      head:[["#","COMPETIDOR","ESTADO"]],

      body :data2,
      startY:110
    });





  /* doc.text("__________________________",10,195)
  doc.text(" NOMBRE Y FIRMA",25,200) */

   // Open PDF document in new tab
     doc.output('dataurlnewwindow')
  }

}
