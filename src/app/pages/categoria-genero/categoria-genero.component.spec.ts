import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriaGeneroComponent } from './categoria-genero.component';

describe('CategoriaGeneroComponent', () => {
  let component: CategoriaGeneroComponent;
  let fixture: ComponentFixture<CategoriaGeneroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriaGeneroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriaGeneroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
