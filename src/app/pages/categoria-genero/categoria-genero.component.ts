import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categoria-genero',
  templateUrl: './categoria-genero.component.html',
  styleUrls: ['./categoria-genero.component.scss']
})
export class CategoriaGeneroComponent implements OnInit {

  public rango = '';
  constructor(public _route: Router,
              public _router:ActivatedRoute) { }

  ngOnInit(): void {
    this.rango = this._router.snapshot.paramMap.get('rango')
  }

  enviar(event){
    if(this.rango == 'Grados kyu'){
      this._route.navigate(['/dashboard/categoria/kyu/',event,this.rango])
    }else{
      this._route.navigate(['/dashboard/categoria/',event,this.rango])
    }
  }

}
