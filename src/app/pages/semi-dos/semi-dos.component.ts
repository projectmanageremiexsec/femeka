import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';

@Component({
  selector: 'app-semi-dos',
  templateUrl: './semi-dos.component.html',
  styleUrls: ['./semi-dos.component.scss']
})
export class SemiDosComponent implements OnInit {

  public datosCompetidorRojo ={
    equipo: "",
    idAtleta : { valores:[],_id:""},
    kata: "",
    nivelAtletico: [],
    nivelTecnico: [],
    puntajeTotal:0,
    ronda: "",
    tatami: "",
    _id:""

  }
  public datosCompetidorAzul ={
    equipo: "",
    idAtleta : { valores:[],_id:""},
    kata: "",
    nivelAtletico: [],
    nivelTecnico: [],
    puntajeTotal:0,
    ronda: "",
    tatami: "",
    _id:""

  }

  public selectDevice='';
  public kata = [];
public semiTatami =""
  public tatami = [];
  public  division ="Ninguna"

  public ganadores = []

  public resultadosSemiDos  = []  ;
  public resultRojo = true;
  public resultAzul = true;

  
  public rango ='';
  public divi="";
  constructor(public _atletas:AtletasService,
    public _router: Router,public routes: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.rango = this.routes.snapshot.paramMap.get('rango');
    this.divi = this.routes.snapshot.paramMap.get('division');
    this.datosCompetidorRojo = JSON.parse(localStorage.getItem('semiDosRojo'));
  this.datosCompetidorAzul = JSON.parse(localStorage.getItem('semiDosAzul'));
  console.log(this.datosCompetidorRojo,"Aqui semi Dos");
  this.katas();
  this.obtenerTatami();
  this.obtenerLocal();
  this.obeterResulatdosSemiDos();
  }



  katas(){
    this._atletas.obtenerKatas().subscribe((resp:any) =>{
      if (resp.ok) {
        this.setKatas(resp['katas']);
      }
    })
  }

  setKatas(katas){
    this.kata = katas
  }



  obtenerTatami(){
    this._atletas.obtenerTatami().subscribe((resp:any) => {
      if(resp.ok){
        this.setTatami(resp['tatamis']);
      }
    })
  }

  setTatami(tatami){
    this.tatami = tatami
  }

  guardarTatamiFinalaka(){
    localStorage.setItem('semifinalTatami',this.semiTatami)
  }

  obtenerLocal(){
    if(localStorage.getItem('semifinalTatami') != null){
      this.semiTatami = localStorage.getItem('semifinalTatami')
    }
  }

obeterResulatdosSemiDos(){
  this._atletas.obtenerResultadosSemiDos().subscribe((data:any)=>{
    console.log(data);
     this.setSemi(data['finalistas']);
  })
}
setSemi(result){
this.resultadosSemiDos = result

result.forEach(element => {
  if(element.idAtleta._id == this.datosCompetidorAzul.idAtleta._id){
      this.resultAzul = false;
    }
    else if (element.idAtleta._id == this.datosCompetidorRojo.idAtleta._id){
       this.resultRojo =false;
    }
});

this.resultadosSemiDos.sort(function(a,b){
  if(a.puntajeTotal < b.puntajeTotal){
    return 1;
  }
  if (a.puntajeTotal > b.puntajeTotal) {
    return -1;
  }
  return 0;
})

// this.ganador = this.resultadosSemiDos[0]

}

  equipoaka(event,ronda){
    this._router.navigate(['/dashboard/competidor/aka/',event,this.selectDevice,this.semiTatami,ronda,this.datosCompetidorRojo.idAtleta.valores[0].colorCinta , this.division])
    
  }
  equipoao(event,ronda){
    // console.log(event,ronda,"aqui estan 2");
    
    this._router.navigate(['/dashboard/competidor/ao/',event,this.selectDevice,this.semiTatami,ronda,this.datosCompetidorAzul.idAtleta.valores[0].colorCinta , this.division])
  }

  redireccion(){
    //   this._atletas.enviarTablaWinner(this.resultadosSemiDos[0]).subscribe((data:any)=>{
    //     console.log(data);
        
    // });
    const win ={
      idAtleta : this.resultadosSemiDos[0].idAtleta._id,
      puntajeTotal: this.resultadosSemiDos[0].puntajeTotal,
      lugar : "Tercer Lugar",
      rango: this.rango,
      division: this.divi
    }
    this._atletas.enviarTablaWinner(win).subscribe((data:any)=>{
      console.log(data);
      
    });
    // this._router.navigate(['/dashboard']);
    localStorage.removeItem('semifinalTatami');
    localStorage.removeItem('semiDosRojo');
    localStorage.removeItem('semiDosAzul');
    this._router.navigate(['/dashboard/registro/competidores']);
  }

}

interface ganador {
  idAtleta: {valores: [{nombre :"",apellidoPaterno :"",estado:""}]},
  nivelAtletico: [],
  nivelTecnico: [],
  puntajeTotal :0
}