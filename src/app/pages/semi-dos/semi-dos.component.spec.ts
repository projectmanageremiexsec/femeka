import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SemiDosComponent } from './semi-dos.component';

describe('SemiDosComponent', () => {
  let component: SemiDosComponent;
  let fixture: ComponentFixture<SemiDosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SemiDosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SemiDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
