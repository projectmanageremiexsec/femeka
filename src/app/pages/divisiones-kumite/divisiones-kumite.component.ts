import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';

@Component({
  selector: 'app-divisiones-kumite',
  templateUrl: './divisiones-kumite.component.html',
  styleUrls: ['./divisiones-kumite.component.scss']
})
export class DivisionesKumiteComponent implements OnInit {

  public divisiones = [];
  public genero = '';
  public rango = '';
  public categoria = '';
  constructor(private _atletas:AtletasService,
              private _route:ActivatedRoute,
              private _router:Router) { }

  ngOnInit(): void {
    this.genero= this._route.snapshot.paramMap.get('genero');
    this.rango= this._route.snapshot.paramMap.get('rango');
    this.categoria= this._route.snapshot.paramMap.get('categoria');
    this.obtenerDivisiones();
  }

  obtenerDivisiones(){
    this._atletas.obtenerDivisiones().subscribe((resp:any) =>{
      this.setDivisiones(resp['Divi']);
    })
  }

  setDivisiones(divisiones){
    this.divisiones = divisiones;
    console.log(this.divisiones);
  }

  enviar(division){
    this._router.navigate(['/dashboard/graficas/kumite/',this.genero,this.categoria,this.rango,division])
  }

}
