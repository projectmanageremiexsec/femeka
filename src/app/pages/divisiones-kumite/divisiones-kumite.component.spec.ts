import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionesKumiteComponent } from './divisiones-kumite.component';

describe('DivisionesKumiteComponent', () => {
  let component: DivisionesKumiteComponent;
  let fixture: ComponentFixture<DivisionesKumiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivisionesKumiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionesKumiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
