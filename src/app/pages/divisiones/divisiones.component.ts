import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from '../../services/atletas/atletas.service';
import { DatosService } from '../../services/datos/datos.service';

@Component({
  selector: 'app-divisiones',
  templateUrl: './divisiones.component.html',
  styleUrls: ['./divisiones.component.scss']
})
export class DivisionesComponent implements OnInit {

  public divisiones = [];
  public genero = '';
  public rango = '';
  public categoria = '';
  constructor(private _atletas:AtletasService,
              private _route:ActivatedRoute,
              private _router:Router,
              private _datos:DatosService) {}

  ngOnInit(): void {
    this.genero= this._route.snapshot.paramMap.get('genero');
    this.rango= this._route.snapshot.paramMap.get('rango');
    this.categoria= this._route.snapshot.paramMap.get('categoria');
    this.obtenerDivisiones();
  }

  obtenerDivisiones(){
    this.divisiones = this._datos.divisiones;
  }

  enviar(division){
    this._router.navigate(['/dashboard/tablas/competencia/',this.genero,this.categoria,this.rango,division])
  }
}
