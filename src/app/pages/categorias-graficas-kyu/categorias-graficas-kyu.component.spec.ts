import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasGraficasKyuComponent } from './categorias-graficas-kyu.component';

describe('CategoriasGraficasKyuComponent', () => {
  let component: CategoriasGraficasKyuComponent;
  let fixture: ComponentFixture<CategoriasGraficasKyuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriasGraficasKyuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasGraficasKyuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
