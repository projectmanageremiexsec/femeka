import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';

@Component({
  selector: 'app-categorias-graficas-kyu',
  templateUrl: './categorias-graficas-kyu.component.html',
  styleUrls: ['./categorias-graficas-kyu.component.scss']
})
export class CategoriasGraficasKyuComponent implements OnInit {

  public genero ='';
  public categorias=[];
  public categoria = '';
  public rango = '';
  public division = ''
  constructor(private _route:ActivatedRoute,
              private _router:Router,
              private _categoria:AtletasService) { }

  ngOnInit(): void {
    this.genero= this._route.snapshot.paramMap.get('genero');
    this.rango= this._route.snapshot.paramMap.get('rango');
    
    this.obtenerCatego();
  }

  obtenerCatego(){
    this._categoria.obtenerCategoriasKyu().subscribe((resp:any) =>{
      if (resp.ok) {
        this.setCategori(resp['grados']);
      }
    })
  }

  setCategori(categorias){
    this.categorias = categorias;
  }

  navegar(categoria){
    this.categoria = categoria
    this._router.navigate(['/dashboard/divisiones/kumite/',this.genero,this.categoria,this.rango])
  }
}
