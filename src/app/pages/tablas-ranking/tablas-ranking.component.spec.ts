import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablasRankingComponent } from './tablas-ranking.component';

describe('TablasRankingComponent', () => {
  let component: TablasRankingComponent;
  let fixture: ComponentFixture<TablasRankingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablasRankingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablasRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
