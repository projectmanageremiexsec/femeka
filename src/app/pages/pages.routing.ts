import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegistroCompeComponent } from './registro-compe/registro-compe.component';
import { CompetidorComponent } from './competidor/competidor.component';
import { GraficasKumiteComponent } from './graficas-kumite/graficas-kumite.component';
import { DocumentosComponent } from './documentos/documentos.component';
import { CompetidorRojoComponent } from './competidor-rojo/competidor-rojo.component';
import { TablasRankingComponent } from './tablas-ranking/tablas-ranking.component';
import { DescargarDocumentComponent } from './descargar-document/descargar-document.component';
import { CategoriaGeneroComponent } from './categoria-genero/categoria-genero.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { TablasCompetenciaComponent } from './tablas-competencia/tablas-competencia.component';
import { CategoriaRangoComponent } from './categoria-rango/categoria-rango.component';
import { DivisionesComponent } from './divisiones/divisiones.component';
import { CategoriasKyuComponent } from './categorias-kyu/categorias-kyu.component';
import { SemiUnoComponent } from './semi-uno/semi-uno.component';
import { SemiDosComponent } from './semi-dos/semi-dos.component';
import { FinalComponent } from './final/final.component';
import { CategoriasGraficasComponent } from './categorias-graficas/categorias-graficas.component';
import { CategoriasRangoKumiteComponent } from './categorias-rango-kumite/categorias-rango-kumite.component';
import { CategoriasGeneroKumiteComponent } from './categorias-genero-kumite/categorias-genero-kumite.component';
import { CategoriasGraficasKyuComponent } from './categorias-graficas-kyu/categorias-graficas-kyu.component';
import { DivisionesKumiteComponent } from './divisiones-kumite/divisiones-kumite.component';
import { GanadoresComponent } from './ganadores/ganadores.component';
import {PoolsComponent} from './pools/pools.component';
import { MostrarAkaComponent } from './mostrar-aka/mostrar-aka.component';
import { MostrarAoComponent } from '../pages/mostrar-ao/mostrar-ao.component';
import { GenerarTablasComponent } from '../pages/generar-tablas/generar-tablas.component';


const routes: Routes = [
    {
        path: 'dashboard',
        component: PagesComponent,
        children: [
            { path: '', component: DashboardComponent, data:{titulo : 'EMI SPORT'} },
            { path: 'registro/competidores', component: RegistroCompeComponent , data:{titulo : 'REGISTRO DE ATLETAS'} },
            { path: 'competidor/ao/:genero/:categoria/:kata/:ronda/:rango/:division/:id', component: CompetidorComponent , data:{titulo : 'COMPETIDOR AO'}},
            { path: 'competidor/aka/:genero/:categoria/:kata/:ronda/:rango/:division/:id', component: CompetidorRojoComponent , data:{titulo : 'COMPETIDOR AKA'}},
            { path: 'tablas/ranking', component: TablasRankingComponent , data:{titulo : 'RANKING'}},
            { path: 'documentos', component: DocumentosComponent , data:{titulo : 'DOCUMENTOS'}},
            { path: 'descargar/documentos/:tipo', component: DescargarDocumentComponent , data:{titulo : 'DOWNLOAD'}},
            { path: 'categoria/rango', component: CategoriaRangoComponent , data:{titulo : 'RANGOS'}},
            { path: 'categoria/genero/:rango', component: CategoriaGeneroComponent , data:{titulo : 'GENERO'}},
            { path: 'categoria/:genero/:rango', component: CategoriasComponent , data:{titulo : 'CATEGORIAS'}},
            { path: 'categoria/kyu/:genero/:rango', component: CategoriasKyuComponent , data:{titulo : 'CATEGORIAS KYU'}},
            { path: 'divisiones/:genero/:categoria/:rango', component: DivisionesComponent , data:{titulo : 'DIVISIONES'}},
            { path: 'tablas/competencia/:genero/:categoria/:rango/:division', component: TablasCompetenciaComponent , data:{titulo : 'COMPETENCIAS'}},
            { path: 'semifinal/uno', component: SemiUnoComponent, data:{titulo: 'SEMIFINAL'}},
            { path: 'semifinal/dos', component: SemiDosComponent, data:{titulo: 'SEMIFINAL'}},
            { path: 'final/:division/:rango', component: FinalComponent, data:{titulo: 'FINAL'}},
            /////////////////////////////////////////* KUMITE */////////////////////////////////////////////////////////////
            { path: 'categorias/rango/kumite', component: CategoriasRangoKumiteComponent, data:{titulo: 'RANGO KUMITE'}},
            { path: 'categorias/genero/kumite/:rango', component: CategoriasGeneroKumiteComponent, data:{titulo: 'GENERO KUMITE'}},
            { path: 'categorias/kumite/:genero/:rango', component: CategoriasGraficasComponent, data:{titulo: 'CATEGORIAS'}},
            { path: 'categorias/kumite/kyu/:genero/:rango', component: CategoriasGraficasKyuComponent, data:{titulo: 'CATEGORIAS KYU'}},
            { path: 'divisiones/kumite/:genero/:categoria/:rango', component: DivisionesKumiteComponent, data:{titulo: 'DIVISIONES KUMITE'}},
            { path: 'graficas/kumite/:genero/:categoria/:rango/:division', component: GraficasKumiteComponent , data:{titulo : 'GRAFICAS KUMITE'}},
            { path: 'ganadores/:genero/:categoria/:rango/:division', component: GanadoresComponent , data:{titulo : 'GANADORES'}},
            { path:'numeros/pools', component :PoolsComponent , data:{titulo : 'TABLA DE ATLETAS'} },
            { path:'mostrar/competidor', component:MostrarAkaComponent, data:{titulo : 'MOSTRAR ATLETA'} },
            { path:'mostrar/competidor/ao', component:MostrarAoComponent, data:{titulo : 'MOSTRAR ATLETA'} },
            { path:'generar/tablas', component:GenerarTablasComponent, data:{titulo : 'TABLAS'} }
        ]
    },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class PagesRoutingModule {}
