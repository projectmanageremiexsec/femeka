import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categorias-genero-kumite',
  templateUrl: './categorias-genero-kumite.component.html',
  styleUrls: ['./categorias-genero-kumite.component.scss']
})
export class CategoriasGeneroKumiteComponent implements OnInit {

  public rango = '';
  constructor(public _route: Router,
              public _router:ActivatedRoute) { }

  ngOnInit(): void {
    this.rango = this._router.snapshot.paramMap.get('rango')
  }

  enviar(event){
    if(this.rango == 'Grados kyu'){
      this._route.navigate(['/dashboard/categorias/kumite/kyu/',event,this.rango])
    }else{
      this._route.navigate(['/dashboard/categorias/kumite/',event,this.rango])
    } 
  }

}
