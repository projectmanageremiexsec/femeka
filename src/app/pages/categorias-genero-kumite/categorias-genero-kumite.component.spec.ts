import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasGeneroKumiteComponent } from './categorias-genero-kumite.component';

describe('CategoriasGeneroKumiteComponent', () => {
  let component: CategoriasGeneroKumiteComponent;
  let fixture: ComponentFixture<CategoriasGeneroKumiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriasGeneroKumiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasGeneroKumiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
