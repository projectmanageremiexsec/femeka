import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficasKumiteComponent } from './graficas-kumite.component';

describe('GraficasKumiteComponent', () => {
  let component: GraficasKumiteComponent;
  let fixture: ComponentFixture<GraficasKumiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraficasKumiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficasKumiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
