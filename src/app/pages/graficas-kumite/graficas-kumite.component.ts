import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AtletasService } from '../../services/atletas/atletas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-graficas-kumite',
  templateUrl: './graficas-kumite.component.html',
  styleUrls: ['./graficas-kumite.component.scss']
})
export class GraficasKumiteComponent implements OnInit {

  clases = {
    rojo:'row rojo',
    azul:'row azul'
  }

  public pas = {
    id:'rojo'
  }
  public pas2 = {
    id:'azul'
  }
  public pass = {
      nombre: 'BYE',
      apellidoP:'',
      apellidoM:'',
      estado:'',
      sexo:'',
      modalidad:'',
      categoria:'',
      peso:0,
      fechaNacimiento:''
  }
  public competidores = [
    /* {
      nombre: 'VICTOR MANUEL',
      apellidoP:'ARAGON',
      apellidoM:'CASTELLANOS',
      estado:'MORELOS',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:85,
      fechaNacimiento:'28/07/1995'
    },
    {
      nombre: 'ISIS',
      apellidoP:'JAIME',
      apellidoM:'VAZQUEZ',
      estado:'ESTADO DE MEXICO',
      sexo:'M',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:58,
      fechaNacimiento:'01/06/1998'
    },
    {
      nombre: 'ILMERS MICHEL',
      apellidoP:'GONZÁLEZ',
      apellidoM:'FLORES',
      estado:'PUEBLA',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:75,
      fechaNacimiento:'10/08/1996'
    },
    {
      nombre: 'PEPE',
      apellidoP:'PECAS',
      apellidoM:'PICA',
      estado:'MORELOS',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:85,
      fechaNacimiento:'28/07/1995'
    },
    {
      nombre: 'CHUCHITA1',
      apellidoP:'PEREZ',
      apellidoM:'PEREZ',
      estado:'GUERRERO',
      sexo:'M',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:58,
      fechaNacimiento:'01/06/1998'
    },
    {
      nombre: 'CHUCHITO1',
      apellidoP:'GOMEZ',
      apellidoM:'GOMEZ',
      estado:'OAXACA',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:75,
      fechaNacimiento:'10/08/1996'
    },
    {
      nombre: 'CHUCHITO2',
      apellidoP:'GOMEZ',
      apellidoM:'GOMEZ',
      estado:'OAXACA',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:75,
      fechaNacimiento:'10/08/1996'
    },
    {
      nombre: 'CHUCHITO3',
      apellidoP:'GOMEZ',
      apellidoM:'GOMEZ',
      estado:'OAXACA',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:75,
      fechaNacimiento:'10/08/1996'
    },
    {
      nombre: 'CHUCHITO4',
      apellidoP:'GOMEZ',
      apellidoM:'GOMEZ',
      estado:'GUERRERO',
      sexo:'H',
      modalidad:'KUMITE',
      categoria:'INFANTIL-A',
      peso:75,
      fechaNacimiento:'10/08/1996'
    } */
  ]
  public numeroCompe;
  public compe=[];
  public octavos=[]
  public cuartos=[];
  public semi=[];
  public final=[];
  public final2=[];
  public atl=[];
  public competidor = [];
  public categoria='';
  public genero = '';
  public rango='';
  public division='';
  constructor(public _atletas:AtletasService,
              public _route:ActivatedRoute,
              public _router: Router) { }

  ngOnInit(): void {
    /* this.arregloAtletas(); */
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero = this._route.snapshot.paramMap.get('genero')
    this.rango = this._route.snapshot.paramMap.get('rango')
    this.division = this._route.snapshot.paramMap.get('division')
    this.obtenerAtletas();
  }

  obtenerAtletas(){
    this._atletas.obtenerAtletas().subscribe((resp:any)=>{
      this.setAtletas(resp['respuesta']);
    })
  }

  setAtletas(atletas){
    console.log(atletas);
    
    atletas.forEach(element => {
      if(element.valores.length != 0){
        if(element.valores[0].modalidad != undefined){
          if(element.valores[0].modalidad == 'Kumite'){
            if(element.valores[0].sexo == this.genero){
              if(element.valores[0].categoria === this.categoria){
                if(element.valores[0].divisiones != null){
                  if(element.valores[0].divisiones === this.division){
                    if(element.valores[0].colorCinta === this.rango){
                      this.competidores.push(element.valores[0])
                    }
                  }
                }else{
                  if(element.valores[0].colorCinta === this.rango){
                    this.competidores.push(element.valores[0])
                  }
                }
              }
            }
          }
        }
      }
    });
    this.inicial();
  }
  /* arreglarArray(){
    let contador=0;
    var j;
    let compet=[]
    this.competidores.sort((a,b) => {
      if(a.estado == b.estado){
        contador = contador + 1
        return 0;
      }
      if(a.estado != b.estado) return -1;
      return 1;
    })    
    for (j = 0; j <= this.competidores.length; j+= contador) {
      compet.push(this.competidores.slice(j, j + contador)); 
    } 
  }  */

  /* arregloAtletas(){
    this._atletas.obtenerAtletas().subscribe((resp:any) => {
        this.setAtletas(resp['respuesta']);        
    });
  }
  setAtletas(arreglo){
      arreglo.forEach(element => {
        this.competidores.push(element.valores[0])
      });
      this.inicial();
  } */

  inicial(){
    this.numeroCompe = this.competidores.length % 2
    if(this.competidores.length != 0){
      if(this.numeroCompe == 0){
        this.numeroCompe = this.competidores.length / 2;
        this.compe = this.grupos(this.competidores, 2); 
        console.log(this.competidor);
        
        this.crearTablasCompe(this.numeroCompe);     
      }else{
        this.competidores.push(this.pass)
        this.numeroCompe= (this.competidores.length /2)
        this.compe = this.grupos(this.competidores, 2);
        this.crearTablasCompe(this.numeroCompe)
      }
    }else{
      Swal.fire({
        title: 'No hay ningun atleta registrado',
        confirmButtonText: `Ok`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this._router.navigate(['/dashboard/categorias/rango/kumite']);
        }
      })  
    }
  }

  grupos(arr, size) {
    var i;
    /* this.arreglarArray(); */ 
    /* this.competidores.sort(function (a, b) {
      if (a.estado != b.estado) {
        return 1;
      }
      if (a.estado == b.estado) {
        return -1;
      }
      return 0;
    }); */
    arr.sort(function(a,b){return a.estado != b.estado;});
    console.log(arr);
    

    for (i = 0; i < arr.length; i+= size) {
      /* console.log(arr.slice(i, i + size)); */
        this.competidor.push(arr.slice(i, i + size)); 
    } 
    console.log(this.competidor);
    
    return this.competidor;
  }

  crearTablasCompe(competidores){
    var contador = 0
    var contador1=0
    var contador2=0
    for (let index = 0; index < competidores; index++) {
      if(contador == 0){
        this.octavos.push(this.pas)
        contador = 1;
      }else{
        this.octavos.push(this.pas2)
        contador = 0;
      }
    }
    if(this.octavos.length != 1){
      for (let index = 0; index < this.octavos.length/2; index++) {
        if(contador1 == 0){
          this.cuartos.push(this.pas)
          contador1 = 1;
        }else{
          this.cuartos.push(this.pas2)
          contador1 = 0;
        }
      }
    }
    
    if(this.cuartos.length != 1){
      for (let index = 0; index < this.cuartos.length/2; index++) {
        if(contador2 == 0){
          this.semi.push(this.pas)
          contador2 = 1;
        }else{
          this.semi.push(this.pas2)
          contador2 = 0;
        }
      } 
    }
       
    if(this.semi.length != 1){
      for (let index = 0; index < this.semi.length/2; index++) {
        this.final.push(this.pas)
      }
    }

    if(this.final.length != 1){
      for (let index = 0; index < this.final.length/2; index++) {
        this.final2.push(this.pas)
      }
    }
  }
}
