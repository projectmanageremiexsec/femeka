import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescargarDocumentComponent } from './descargar-document.component';

describe('DescargarDocumentComponent', () => {
  let component: DescargarDocumentComponent;
  let fixture: ComponentFixture<DescargarDocumentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescargarDocumentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescargarDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
