import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-descargar-document',
  templateUrl: './descargar-document.component.html',
  styleUrls: ['./descargar-document.component.scss']
})
export class DescargarDocumentComponent implements OnInit {

  public tipo;

  constructor(private _route:ActivatedRoute) { }

  ngOnInit(): void {
    this.tipo = this._route.snapshot.paramMap.get('tipo')
  }

}
