import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';



@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.scss']
})
export class FinalComponent implements OnInit {

  public datosCompetidorRojo ={
    equipo: "",
    idAtleta : { valores:[],_id:""},
    kata: "",
    nivelAtletico: [],
    nivelTecnico: [],
    puntajeTotal:0,
    ronda: "",
    tatami: "",
    _id:""

  }
  public datosCompetidorAzul ={
    equipo: "",
    idAtleta : { valores:[],_id:""},
    kata: "",
    nivelAtletico: [],
    nivelTecnico: [],
    puntajeTotal:0,
    ronda: "",
    tatami: "",
    _id:""

  }

  public selectDevice='';
  public kata = [];
public semiTatami =""
  public tatami = [];
  public  division ="Ninguna"

  public resultadosSemiDos =[];
  public resultRojo = true;
  public resultAzul = true;


  public rango ='';
  public divi="";
  constructor( public _atletas:AtletasService,
    public _router: Router, public routes: ActivatedRoute) { }

  ngOnInit(): void {

    this.rango = this.routes.snapshot.paramMap.get('rango');
    this.divi = this.routes.snapshot.paramMap.get('division');
    this.datosCompetidorRojo = JSON.parse(localStorage.getItem('finalRojo'));
    this.datosCompetidorAzul = JSON.parse(localStorage.getItem('finalAzul'));
    console.log(this.datosCompetidorRojo,"Aqui esta la finalistas");
    this.katas();
    this.obtenerTatami();
    this.obtenerLocal();
    this.obeterResulatdosFinal();

  }

  katas(){
    this._atletas.obtenerKatas().subscribe((resp:any) =>{
      if (resp.ok) {
        this.setKatas(resp['katas']);
      }
    })
  }

  setKatas(katas){
    this.kata = katas
  }



  obtenerTatami(){
    this._atletas.obtenerTatami().subscribe((resp:any) => {
      if(resp.ok){
        this.setTatami(resp['tatamis']);
      }
    })
  }

  setTatami(tatami){
    this.tatami = tatami
  }

  guardarTatamiFinalaka(){
    localStorage.setItem('semifinalTatami',this.semiTatami)
  }

  obtenerLocal(){
    if(localStorage.getItem('semifinalTatami') != null){
      this.semiTatami = localStorage.getItem('semifinalTatami')
    }
  }

obeterResulatdosFinal(){
  this._atletas.obtenerResultadosFinal().subscribe((data:any)=>{
    console.log(data);
     this.setSemi(data['finalistas']);
  })
}
setSemi(result){
this.resultadosSemiDos = result

result.forEach(element => {
console.log(element ,"1");
console.log(this.datosCompetidorRojo.idAtleta._id,"2");



  if(element.idAtleta._id == this.datosCompetidorAzul.idAtleta._id){
      this.resultAzul = false;
    }
    else if (element.idAtleta._id == this.datosCompetidorRojo.idAtleta._id){
       this.resultRojo =false;
    }

});

this.resultadosSemiDos.sort(function(a,b){
  if(a.puntajeTotal < b.puntajeTotal){
    return 1;
  }
  if (a.puntajeTotal > b.puntajeTotal) {
    return -1;
  }
  return 0;
})

// this.ganador = this.resultadosSemiDos[0]

}

  equipoaka(event,ronda){
    this._router.navigate(['/dashboard/competidor/aka/',event,this.selectDevice,this.semiTatami,ronda,this.datosCompetidorRojo.idAtleta.valores[0].colorCinta , this.division])
    
  }
  equipoao(event,ronda){
    // console.log(event,ronda,"aqui estan 2");
    
    this._router.navigate(['/dashboard/competidor/ao/',event,this.selectDevice,this.semiTatami,ronda,this.datosCompetidorAzul.idAtleta.valores[0].colorCinta , this.division])
  }





redireccion(){

  const win ={
    idAtleta : this.resultadosSemiDos[0].idAtleta._id,
    puntajeTotal: this.resultadosSemiDos[0].puntajeTotal,
    lugar : "Primer Lugar",
    rango: this.rango,
  division: this.divi

  }
  this._atletas.enviarTablaWinner(win).subscribe((data:any)=>{
    console.log(data);
    
  });

  const winDos ={
    idAtleta : this.resultadosSemiDos[1].idAtleta._id,
    puntajeTotal: this.resultadosSemiDos[1].puntajeTotal,
    lugar : "Segundo Lugar",
    rango: this.rango,
  division: this.divi

  }
  this._atletas.enviarTablaWinner(winDos).subscribe((data:any)=>{
    console.log(data);
    
  });
  
  console.log(this.resultadosSemiDos[0].idAtleta.valores[0].categoria,);
  const cate ={
    categoria : this.resultadosSemiDos[0].idAtleta.valores[0].categoria,
    rango : this.resultadosSemiDos[0].idAtleta.valores[0].colorCinta,
    division:"",
    genero: this.resultadosSemiDos[0].idAtleta.valores[0].sexo
  }
  this._atletas.enviarTablaCategoriaTerminada(cate).subscribe((data:any)=>{
    console.log(data,"esto envio a tabla winners");
    
  });
  localStorage.removeItem('finalTatami');
  localStorage.removeItem('finalRojo');
  localStorage.removeItem('finalAzul');
  this._router.navigate(['/dashboard/registro/competidores']);
}



}
