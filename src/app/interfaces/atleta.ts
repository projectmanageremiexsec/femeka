export interface Competidor {
  competidoresAKA:{
    ronda1:any[],
    ronda2:any[],
    ronda3:any[],
    ronda4:any[]
  }
  competidoresAO:{
    ronda1:any[],
    ronda2:any[],
    ronda3:any[],
    ronda4:any[]
  }
}

export interface AtletaAka{
  atletasAka:any[]
  atletasAo:any[]
}

export interface Semifinal1{
  atletas:any[]
}

export interface Semifinal2{
  atletas:any[]
}

export interface final{
  atletas:any[]
}

export interface ResultSemi1 {
  resultados:any[]
}

export interface ResultSemi2 {
  resultados:any[]
}

export interface ResultFinal {
  resultados:any[]
}
