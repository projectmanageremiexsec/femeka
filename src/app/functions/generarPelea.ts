export function generar (competidores:any[]){
  let array:any[] = []
  if (competidores.length == 4) {
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[3].nombre)})
    array.push({pelea:competidores[1].nombre.concat(" VS " + competidores[2].nombre)})
    array.push({pelea:competidores[2].nombre.concat(" VS " + competidores[3].nombre)})
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[2].nombre)})
    array.push({pelea:competidores[1].nombre.concat(" VS " + competidores[3].nombre)})
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[1].nombre)})
    return array;
  } else if (competidores.length == 5){
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[1].nombre)})
    array.push({pelea:competidores[1].nombre.concat(" VS " + competidores[2].nombre)})
    array.push({pelea:competidores[2].nombre.concat(" VS " + competidores[3].nombre)})
    array.push({pelea:competidores[3].nombre.concat(" VS " + competidores[4].nombre)})
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[3].nombre)})
    array.push({pelea:competidores[1].nombre.concat(" VS " + competidores[4].nombre)})
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[2].nombre)})
    array.push({pelea:competidores[2].nombre.concat(" VS " + competidores[4].nombre)})
    array.push({pelea:competidores[1].nombre.concat(" VS " + competidores[3].nombre)})
    array.push({pelea:competidores[0].nombre.concat(" VS " + competidores[4].nombre)})
    return array;
  }
}
