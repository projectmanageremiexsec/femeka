import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosService } from '../../services/datos/datos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import AtletasCompe from '../../atletas/nuevoAtleta';
import { AtletasService } from '../../services/atletas/atletas.service';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { AtletaAka, Competidor} from 'src/app/interfaces/atleta';
import { final } from '../../interfaces/atleta';


@Component({
  selector: 'app-categoria10',
  templateUrl: './categoria10.component.html',
  styleUrls: ['./categoria10.component.scss']
})
export class Categoria10Component implements OnInit {

  @Input() categoria = '';
  @Input() genero = '';
  @Input() rango = '';
  @Input() division = '';
  public teamRojo = [];
  public teamAzul = [];
  public teamSemi1 = [];
  public teamSemi2 = [];
  public teamFinal = [];
  public kata = [];
  public selectKata='';
  public tataAka = '';
  public tataAo = '';
  public tatami = [];
  public resultados = [];
  public resultadosRojos = [];
  public resultadosAzules = [];
  public resultadosSemi1 = [];
  public resultadosSemi2 = [];
  public resultadosFinal =[];
  public resultados1 = [];
  public resultados2 = [];
  public res=false;
  public numeroAtletas = 0;
  public semi1 = true;
  public semi2 = true;
  public vueltas = 0;
  public idCompe='';
  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  @ViewChild('modalContent2', { static: true }) modalContent2?: TemplateRef<any>;
  @ViewChild('modalContentCriterios', { static: true }) modalContentDesempate?: TemplateRef<any>;
  @ViewChild('modalContentCompeAka', { static: true }) modalContentCompeAka?: TemplateRef<any>;
  @ViewChild('modalContentSemi1', { static: true }) modalContentSemi1?: TemplateRef<any>;
  @ViewChild('modalContentSemi2', { static: true }) modalContentSemi2?: TemplateRef<any>;
  @ViewChild('modalContentFinal', { static: true }) modalContentFinal?: TemplateRef<any>;
  public atletaAka:AtletaAka = {} as AtletaAka;
  public result:Competidor = {} as Competidor;
  public validSemi1 = false;
  public validSemi2 = false;
  public validFinal = false;

  constructor(public _route:ActivatedRoute,
              public _router: Router,
              public _datos:DatosService,
              public _compe:AtletasService,
              private modal: NgbModal){}

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero =  this._route.snapshot.paramMap.get('genero')
    this.rango =  this._route.snapshot.paramMap.get('rango')
    this.division = this._route.snapshot.paramMap.get('division')
    this.katas();
    this.obtenerCompetencia();
  }

  updateVueltas(){
    let data ={
      vueltas:this.vueltas,
      equipoAKA:this.teamRojo,
      equipoAO:this.teamAzul
    }
    this._compe.actualizarCompetencia(this.idCompe,data).subscribe((resp:any)=>{
      console.log(resp);
    })
  }

  obtenerCompetencia(){
    let val = {
      categoria:this.categoria,
      genero:this.genero,
      grado:this.rango
    }
    console.log('entro');

    this._compe.obtenerCompetencias(val).subscribe((resp:any)=>{
      if(resp.ok){
        if(resp.respuesta.length != 0){
          this.idCompe = resp.respuesta[0]._id
          this.teamRojo = resp.respuesta[0].equipoAKA
          this.teamAzul = resp.respuesta[0].equipoAO
          this.teamSemi1 = resp.respuesta[0].atletasSemi1
          this.teamSemi2 = resp.respuesta[0].atletasSemi2 ///////////////////
          this.teamFinal = resp.respuesta[0].atletasFinal ///////////////////
          this.vueltas = resp.respuesta[0].vueltas
          this.result = resp.respuesta[0].resultados[0]
          this.resultadosSemi1 = resp.respuesta[0].semifinal1
          this.resultadosSemi2 = resp.respuesta[0].semifinal2 ///////////
          this.resultadosFinal = resp.respuesta[0].final ///////////
          if(this.resultadosSemi1 != undefined){
            if (this.resultadosSemi1.length > 0) {
              localStorage.setItem('resultSemi1',JSON.stringify(this.resultadosSemi1))
            }
          }
          if(this.resultadosSemi2 != undefined){
            if (this.resultadosSemi2.length > 0) {
              localStorage.setItem('resultSemi2',JSON.stringify(this.resultadosSemi2))
            }
          }
          if(this.resultadosFinal != undefined){
            if (this.resultadosFinal.length > 0) {
              localStorage.setItem('resultFinal',JSON.stringify(this.resultadosFinal))
            }
          }
          if(this.result != undefined){
            localStorage.setItem('competidores',JSON.stringify(this.result))
          }
          this.setCompetidores(this.teamRojo,this.teamAzul);
        }
      }
    })
  }

  setCompetidores(teamRojo:any, teamAzul:any){
    this.atletaAka.atletasAka = teamRojo
    this.atletaAka.atletasAo = teamAzul
    localStorage.setItem('registros', JSON.stringify(this.atletaAka))
    this.obtenerResultados();
    this.obtenerResultadosSemi();
    this.obtenerResultadosSem2();
    this.obtenerResultadosFinal();
  }

  agregarCompetencia(){
    let datos = {
      categoria:this.categoria,
      genero:this.genero,
      grado:this.rango,
      vueltas:this.vueltas,
      equipoAKA:this.teamRojo,
      equipoAO:this.teamAzul
    }
    this._compe.agregarCompetencia(datos).subscribe((resp:any)=>{
      this.alertaAgregarCompe();
      this.obtenerCompetencia();
    })
  }

  alertaAgregarCompe(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: 'Se agrego la competencia'
    })
  }

  obtenerAtletas(){
    if(localStorage.getItem('registros') != null){
      this.teamRojo = JSON.parse(localStorage.getItem('registros')).atletasAka;
      this.teamAzul = JSON.parse(localStorage.getItem('registros')).atletasAo;
    }
  }

  //////////// proceso para las semifinales y finales ////////////////////////////////////////

  obtenerAtletasSemi1(){
    if(localStorage.getItem('semi1') != null){
      this.teamSemi1 = JSON.parse(localStorage.getItem('semi1')).atletas;
      let datos = {
        atletasSemi1:this.teamSemi1
      }
      this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
        console.log(resp);
      })
    }
  }

  obtenerAtletasSemi2(){
    console.log(this.idCompe,"aaaaaaaaaaaquuuuuuiiiiiiii");

    if(localStorage.getItem('semi2') != null){
      this.teamSemi2 = JSON.parse(localStorage.getItem('semi2')).atletas;
      let datos = {
        atletasSemi2:this.teamSemi2
      }
      console.log(datos,"tiene team");

      this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
        console.log(resp);
      })
    }
  }

  obtenerAtletasFinal(){
    if(localStorage.getItem('final') != null){
      this.teamFinal = JSON.parse(localStorage.getItem('final')).atletas;
      let datos = {
        atletasFinal:this.teamFinal
      }
      this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
        console.log(resp);
      })
    }
  }

  obtenerResultados(){
    if(this.result != undefined){
      let rondaAka2 = this.result.competidoresAKA.ronda2;
      let rondaAka3 = this.result.competidoresAKA.ronda3;
      let rondaAka4 = this.result.competidoresAKA.ronda4;
      this.resultadosRojos = this.result.competidoresAKA.ronda1;
      if(rondaAka2.length != 0){
        this.resultadosRojos.forEach(element => {
          let encontrar = rondaAka2.find(ele => ele.atleta._id == element.atleta._id)
          if(encontrar != undefined){
            element.calificacionFinal = element.calificacionFinal + encontrar.calificacionFinal
          }
        })
      }

      if(rondaAka3.length != 0){
        this.resultadosRojos.forEach(element => {
          let encontrar = rondaAka3.find(ele => ele.atleta._id == element.atleta._id)
          if(encontrar != undefined){
            element.calificacionFinal = element.calificacionFinal + encontrar.calificacionFinal
          }
        })
      }

      if(rondaAka4.length != 0){
        this.resultadosRojos.forEach(element => {
          let encontrar = rondaAka4.find(ele => ele.atleta._id == element.atleta._id)
          if(encontrar != undefined){
            element.calificacionFinal = element.calificacionFinal + encontrar.calificacionFinal
          }
        })
      }

      this.resultadosRojos.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })

      let rondaAo2 = this.result.competidoresAO.ronda2;
      let rondaAo3 = this.result.competidoresAO.ronda3;
      let rondaAo4 = this.result.competidoresAO.ronda4;
      this.resultadosAzules = this.result.competidoresAO.ronda1;

      if(rondaAo2.length != 0){
        this.resultadosAzules.forEach(element => {
          let encontrar = rondaAo2.find(ele => ele.atleta._id == element.atleta._id)
          if(encontrar != undefined){
            element.calificacionFinal = element.calificacionFinal + encontrar.calificacionFinal
          }
        })
      }

      if(rondaAo3.length != 0){
        this.resultadosAzules.forEach(element => {
          let encontrar = rondaAo3.find(ele => ele.atleta._id == element.atleta._id)
          if(encontrar != undefined){
            element.calificacionFinal = element.calificacionFinal + encontrar.calificacionFinal
          }
        })
      }

      if(rondaAo4.length != 0){
        this.resultadosAzules.forEach(element => {
          let encontrar = rondaAo4.find(ele => ele.atleta._id == element.atleta._id)
          if(encontrar != undefined){
            element.calificacionFinal = element.calificacionFinal + encontrar.calificacionFinal
          }
        })
      }
      this.resultadosAzules.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }

  async abrirModal(){
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  async abrirModal2(){
    this.modal.open(this.modalContent2, { size: 'lg' });
  }
  async abrirModalDesempate(){
    this.modal.open(this.modalContentDesempate, { size: 'lg' });
  }

  async abrirModalSemi1(){
    this.modal.open(this.modalContentSemi1, { size: 'lg' });
  }


  async abrirModalSemi2(){
    this.modal.open(this.modalContentSemi2, { size: 'lg' });
  }
  async abrirModalFinal(){
    this.modal.open(this.modalContentFinal, { size: 'lg' });
  }
  abrirModalComAka(item:any){
    localStorage.setItem('mostrarCompe',JSON.stringify(item))
    this._router.navigate(['/dashboard/mostrar/competidor']);
  }

  abrirModalComAo(item:any){
    localStorage.setItem('mostrarCompe',JSON.stringify(item))
    this._router.navigate(['/dashboard/mostrar/competidor/ao']);
  }

  agregarAtleta(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria, _id: Math.random().toString(36).substring(2),rondas:0});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAka(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  eliminarAtletaAka(posicion:any){
    this.teamRojo.forEach((item, index) => {
      if (index  === posicion ) {
        this.teamRojo.splice( index, 1 )
      }
    });
    let data ={
      equipoAKA:this.teamRojo
    }
    this._compe.actualizarCompetencia(this.idCompe,data).subscribe((resp:any)=>{
      console.log(resp);
    })
    localStorage.removeItem('registros')
    this.obtenerCompetencia();
  }

  agregarAtletaAo(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria, _id: Math.random().toString(36).substring(2),rondas:0});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAo(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  agregarAtletaSemi1(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria, _id: Math.random().toString(36).substring(2)});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAkaSemi1(data);
    this.obtenerAtletasSemi1();
    this.modal.dismissAll();
  }


  agregarAtletaSemi2(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria, _id: Math.random().toString(36).substring(2)});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAoSemi2(data);
    this.obtenerAtletasSemi2();
    this.modal.dismissAll();
  }
  agregarAtletaFinal(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria, _id: Math.random().toString(36).substring(2)});
    let compe = new AtletasCompe();
    compe.agregarAtletaFinal(data);
    this.obtenerAtletasFinal();
    this.modal.dismissAll();
  }

  eliminarAtletaAo(posicion:any){
    this.teamAzul.forEach((item, index) => {
      if (index  === posicion ) {
        this.teamAzul.splice( index, 1 )
      }
    });
    let data ={
      equipoAO:this.teamAzul
    }
    this._compe.actualizarCompetencia(this.idCompe,data).subscribe((resp:any)=>{
      console.log(resp);
    })
    localStorage.removeItem('registros')
    this.obtenerCompetencia();
  }

  katas(){
    this.kata = this._datos.katas
  }

  equipoaka(event,index,kata){
    this.teamRojo[index] = Object.assign(event,{kata:kata,rondas:event.rondas+1})
    let datos = {
      categoria:this.categoria,
      genero:this.genero,
      grado:this.rango,
      vueltas:this.vueltas,
      equipoAKA:this.teamRojo,
      equipoAO:this.teamAzul
    }

    if(event.rondas <= this.vueltas){
      this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
        console.log(resp);
      })
    }else{
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Se terminaron las vueltas'
      })
    }
    /* localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/aka/',this.selectKata,'1',this.rango,this.division]) */
  }

  madarCompetidorAka(atleta){
    if(atleta.rondas <= this.vueltas){
      localStorage.setItem('atleta', JSON.stringify(atleta))
      this._router.navigate(['/dashboard/competidor/aka/',this.genero,this.categoria,atleta.kata,atleta.rondas,this.rango,this.division,this.idCompe])
    }else{
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Se terminaron las vueltas'
      })
    }
  }

  equipoao(event,index,kata){
    this.teamAzul[index] = Object.assign(event,{kata:kata,rondas:event.rondas+1})
    let datos = {
      categoria:this.categoria,
      genero:this.genero,
      grado:this.rango,
      vueltas:this.vueltas,
      equipoAKA:this.teamRojo,
      equipoAO:this.teamAzul
    }

    if(event.rondas <= this.vueltas){
      this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
        console.log(resp);
      })
    }else{
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Se terminaron las vueltas'
      })
    }
    /* localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/ao/',this.selectKata,'1',this.rango,this.division]) */
  }

  madarCompetidorAo(atleta){
    if(atleta.rondas <= this.vueltas){
      localStorage.setItem('atleta', JSON.stringify(atleta))
      this._router.navigate(['/dashboard/competidor/ao/',this.genero,this.categoria,atleta.kata,atleta.rondas,this.rango,this.division,this.idCompe])
    }else{
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      Toast.fire({
        icon: 'error',
        title: 'Se terminaron las vueltas'
      })
    }
  }

  subirPunto(item:any){
    let completo = JSON.parse(localStorage.getItem('competidores'))
    let valor = JSON.parse(localStorage.getItem('competidores')).competidoresAKA
    let encontrar = valor.ronda1.findIndex(element => element.atleta._id == item.atleta._id)
    if (encontrar != undefined) {
      valor.ronda1[encontrar].calificacionFinal = valor.ronda1[encontrar].calificacionFinal + 1
    }
    completo.competidoresAKA.ronda1[encontrar].calificacionFinal = valor.ronda1[encontrar].calificacionFinal
    let valores = {
      resultados:completo
    }
    this._compe.actualizarCompetencia(this.idCompe,valores).subscribe((resp:any)=>{
      this.result = resp.respuestaUp.resultados[0]
      if(this.result != undefined){
        localStorage.setItem('competidores',JSON.stringify(this.result))
      }
      this.obtenerResultados();
    })

  }

  subirPuntoAzul(item:any){
    let completo = JSON.parse(localStorage.getItem('competidores'))
    let valor = JSON.parse(localStorage.getItem('competidores')).competidoresAO
    let encontrar = valor.ronda1.findIndex(element => element.atleta._id == item.atleta._id)
    if (encontrar != undefined) {
      valor.ronda1[encontrar].calificacionFinal = valor.ronda1[encontrar].calificacionFinal + 1
    }
    completo.competidoresAO.ronda1[encontrar].calificacionFinal = valor.ronda1[encontrar].calificacionFinal
    let valores = {
      resultados:completo
    }
    this._compe.actualizarCompetencia(this.idCompe,valores).subscribe((resp:any)=>{
      this.result = resp.respuestaUp.resultados[0]
      if(this.result != undefined){
        localStorage.setItem('competidores',JSON.stringify(this.result))
      }
      this.obtenerResultados();
    })

  }


  semiUno(){
    this.validSemi1 = true
    this.validSemi2=false
  }
  semiDos(){
    // localStorage.setItem('semiDosRojo',JSON.stringify(this.resultadosRojos[2]));
    // localStorage.setItem('semiDosAzul',JSON.stringify(this.resultadosAzules[1]));
    // this._router.navigate(['/dashboard/semifinal/dos']);
    this.validSemi1 =false
    this.validSemi2 = true
  }
  final(){
    this.validSemi1 =false
    this.validSemi2 = false
    this.validFinal =true
    // localStorage.setItem('finalRojo',JSON.stringify(this.resultadosRojos[0]));
    // localStorage.setItem('finalAzul',JSON.stringify(this.resultadosAzules[0]));
    // this._router.navigate(['/dashboard/final/', this.division,this.rango]);
  }

  // ACTUALIZAR CATA SEMI 1 sem2 y final
  actualizarCataSemi1(event,index,kata){
    this.teamSemi1[index] = Object.assign(event,{kata:kata})
    let datos = {
      atletasSemi1:this.teamSemi1
    }

    this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
      console.log(resp);
    })
  }
  actualizarCataSemi2(event,index,kata){
    this.teamSemi2[index] = Object.assign(event,{kata:kata})
    let datos = {
      atletasSemi2:this.teamSemi2
    }

    this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
      console.log(resp);
    })
  }

  actualizarCataFinal(event,index,kata){
    this.teamFinal[index] = Object.assign(event,{kata:kata})
    let datos = {
      atletasFinal:this.teamFinal
    }

    this._compe.actualizarCompetencia(this.idCompe,datos).subscribe((resp:any)=>{
      console.log(resp);
    })
  }

  // ENVIAR COMPETIDOR
  madarCompetidorSemi1(atleta){
    localStorage.setItem('atleta', JSON.stringify(atleta))
    this._router.navigate(['/dashboard/competidor/aka/',this.genero,this.categoria,atleta.kata,'semi1',this.rango,this.division,this.idCompe])
  }
  madarCompetidorSemi2(atleta){
    localStorage.setItem('atleta', JSON.stringify(atleta))
    this._router.navigate(['/dashboard/competidor/aka/',this.genero,this.categoria,atleta.kata,'semi2',this.rango,this.division,this.idCompe])
  }
  madarCompetidorFinal(atleta){
    localStorage.setItem('atleta', JSON.stringify(atleta))
    this._router.navigate(['/dashboard/competidor/aka/',this.genero,this.categoria,atleta.kata,'final',this.rango,this.division,this.idCompe])
  }

  // COMPARAR CALIFICACION
  obtenerResultadosSemi(){
    if(this.resultadosSemi1 != undefined){
      this.resultadosSemi1.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }
  obtenerResultadosSem2(){
    if(this.resultadosSemi2 != undefined){
      this.resultadosSemi2.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }
  obtenerResultadosFinal(){
    if(this.resultadosFinal != undefined){
      this.resultadosFinal.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }

  //SUBIR PUNTO SEMI1 semi2 y final
  subirPuntoSemi1(item:any){
    let valor = JSON.parse(localStorage.getItem('resultSemi1'))
    let encontrar = valor.findIndex(element => element.atleta._id == item.atleta._id)
    if (encontrar != undefined) {
      valor[encontrar].calificacionFinal = valor[encontrar].calificacionFinal + 1
    }

    let valores = {
      semifinal1:valor
    }
    this._compe.actualizarCompetencia(this.idCompe,valores).subscribe((resp:any)=>{
      this.resultadosSemi1 = resp.respuestaUp.semifinal1
      if(this.resultadosSemi1 != undefined){
        localStorage.setItem('resultSemi1',JSON.stringify(this.resultadosSemi1))
      }
      this.obtenerResultadosSemi();
    })

  }

  subirPuntoSemi2(item:any){
    let valor = JSON.parse(localStorage.getItem('resultSemi2'))
    let encontrar = valor.findIndex(element => element.atleta._id == item.atleta._id)
    if (encontrar != undefined) {
      valor[encontrar].calificacionFinal = valor[encontrar].calificacionFinal + 1
    }

    let valores = {
      semifinal2:valor
    }
    this._compe.actualizarCompetencia(this.idCompe,valores).subscribe((resp:any)=>{
      this.resultadosSemi2 = resp.respuestaUp.semifinal2
      if(this.resultadosSemi2 != undefined){
        localStorage.setItem('resultSemi2',JSON.stringify(this.resultadosSemi2))
      }
      this.obtenerResultadosSem2();
    })

  }

  subirPuntoFinal(item:any){
    let valor = JSON.parse(localStorage.getItem('resultFinal'))
    let encontrar = valor.findIndex(element => element.atleta._id == item.atleta._id)
    if (encontrar != undefined) {
      valor[encontrar].calificacionFinal = valor[encontrar].calificacionFinal + 1
    }

    let valores = {
      final:valor
    }
    this._compe.actualizarCompetencia(this.idCompe,valores).subscribe((resp:any)=>{
      this.resultadosFinal = resp.respuestaUp.final
      if(this.resultadosFinal != undefined){
        localStorage.setItem('resultFinal',JSON.stringify(this.resultadosFinal))
      }
      this.obtenerResultadosFinal();
    })

  }


}
