import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria10Component } from './categoria10.component';

describe('Categoria10Component', () => {
  let component: Categoria10Component;
  let fixture: ComponentFixture<Categoria10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
