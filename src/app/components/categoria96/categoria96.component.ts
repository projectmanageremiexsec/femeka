import { Component,Input,OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AtletasService } from 'src/app/services/atletas/atletas.service';

@Component({
  selector: 'app-categoria96',
  templateUrl: './categoria96.component.html',
  styleUrls: ['./categoria96.component.scss']
})
export class Categoria96Component implements OnInit {

  @Input() categoria = '';
  @Input() genero = '';
  @Input() rango = '';
  @Input() division = '';

  public botontatami = true;
  public atletas = [];
  public teamRojo = [];
  public teamAzul = [];
  public kata = [];
  public selectDevice='';
  public tataAka = '';
  public tataAo = '';
  public tatami = [];
  public resultados = [];
  public resultadosRonda1Rojos = [];
  public resultadosRonda1Rojos2 = [];

  public resultadosRonda1Azules = [];
  public resultadosRonda1Azules2 = [];

  public resultadosRonda2Rojos = [];
  public resultadosRonda2Azules = [];

  public resultadosRonda3Rojos =[];
  public resultadosRonda3Azules =[];

  public ronda1Aka = [];
  public ronda1Ao = [];
  public ronda2Aka = [];
  public ronda2Ao = [];
  public ronda3Aka = [];
  public ronda3Ao = [];

  public teamRojo1 = [];
  public teamRojo2 = [];
  public teamRojo3 = [];
  public teamRojo4 = [];


public rojo1 =[] ;
public rojo2 =[];

public azul1 =[];
public azul2=[];

  public teamAzul1 = [];
  public teamAzul2 = [];
  public teamAzul3 = [];
  public teamAzul4 = [];

  public res=false;
  public numeroAtletas = 0;
  public semi1 = true;
  public semi2 = true;
  public ronda1 = 'ronda1';
  public ronda2 = 'ronda2';
  public ronda3 = 'ronda3';
  public ronda4 = 'ronda4';
  // public ronda5 = 'ronda5';
  // public ronda6 = 'ronda6';
  constructor(public _route:ActivatedRoute,
    public _atletas:AtletasService,
    public _router: Router) { }

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero =  this._route.snapshot.paramMap.get('genero')
    this.rango = this._route.snapshot.paramMap.get('rango')
    this.division =  this._route.snapshot.paramMap.get('division')
    this.katas();
    this.obtenerAtletas();
    this.obtenerTatami();
    this.obtenerResultados();
    this.obtenerLocal();
  }



  obtenerLocal(){
    if(localStorage.getItem('tatamiAka') != null){
      this.tataAka = localStorage.getItem('tatamiAka')
    }
    if(localStorage.getItem('tatamiAo') != null){
      this.tataAo = localStorage.getItem('tatamiAo')
    }
  }

  guardarTatamiAkaLocal(){
    localStorage.setItem('tatamiAka',this.tataAka)
  }

  guardarTatamiAoLocal(){
    localStorage.setItem('tatamiAo', this.tataAo)
  }

  obtenerAtletas(){
    this._atletas.obtenerAtletas().subscribe((resp:any)=>{
      this.setAtletas(resp['respuesta']);
    })
  }

  setAtletas(atletas){
    atletas.forEach(element => {
      if(element.valores.length != 0){
        if(element.valores[0].modalidad != null){
          if (element.valores[0].modalidad == 'Kata') {
            if(element.valores[0].integrantes == 'Individual'){
              if(element.valores[0].sexo == this.genero){
                if(element.valores[0].categoria === this.categoria){
                  if(element.valores[0].divisiones != null){
                    if(element.valores[0].divisiones === this.division){
                      if(element.valores[0].colorCinta === this.rango){
                        this.atletas.push(element)
                      }
                    }
                  }else{
                    if(element.valores[0].colorCinta === this.rango){
                      this.atletas.push(element)
                    }
                  }
                }
              }
            }
          }
        }
      }
    });
    this.equipos(this.atletas);
  }

  equipos(atletas){
    this.numeroAtletas = atletas.length;
    if(this.numeroAtletas != 5){
      this.semi1 = false;
      this.semi2 = false;
    }
    if (atletas.length == 2 || atletas.length == 3) {
      atletas.forEach(element => {
        this.teamRojo.push(element)
      });
      this.teamRojo.sort(function() { return Math.random() - 0.5 });
    } else {
      /* atletas.sort(function() { return Math.random() - 0.5 }); */
      var index = atletas.length;
      this.teamRojo = atletas.slice(index/2,index);
      this.teamAzul = atletas.slice(0, index/2);
      var indexRojo = this.teamRojo.length
      var indexAzul = this.teamAzul.length
      this.rojo1 = this.teamRojo.slice(indexRojo/2,indexRojo);
      this.rojo2 = this.teamRojo.slice(0, indexRojo/2);
      this.azul1 = this.teamAzul.slice(indexAzul/2,indexAzul);
      this.azul2 = this.teamAzul.slice(0, indexAzul/2);   
      
      var indexR = this.rojo1.length
      var indexR2 = this.rojo2.length
      
      this.teamRojo1 = this.rojo1.slice(indexR/2,indexR);
      this.teamRojo2 = this.rojo1.slice(0,indexR/2)
      this.teamRojo3 = this.rojo2.slice(indexR2/2,indexR2);
      this.teamRojo4 = this.rojo2.slice(0,indexR2/2);


      var indexA = this.azul1.length;
      var indexA2 = this.azul2.length;


      this.teamAzul1 = this.azul1.slice(indexA/2,indexA);
      this.teamAzul2 = this.azul1.slice(0,indexA/2);
      this.teamAzul3 = this.azul2.slice(indexA2/2,indexA2);
      this.teamAzul4 = this.azul2.slice(0,indexA2/2);
    }
  }

  katas(){
    this._atletas.obtenerKatas().subscribe((resp:any) =>{
      if (resp.ok) {
        this.setKatas(resp['katas']);
      }
    })
  }

  setKatas(katas){
    this.kata = katas
  }

  obtenerTatami(){
    this._atletas.obtenerTatami().subscribe((resp:any) => {
      if(resp.ok){
        this.setTatami(resp['tatamis']);
      }
    })
  }

  setTatami(tatami){
    this.tatami = tatami
  }

  obtenerResultados(){
    this._atletas.obtenerResultados().subscribe((resp:any) => {
      if (resp.ok) {
        this.setResultados(resp['finalistas'], true);
      }else{
        this.resultados=[]
        this.setResultados(this.resultados, false);
      }
    })
  }

  setResultados(res,status){
    console.log(res);
    
    res.forEach(element => {
      if(element.idAtleta.valores[0].categoria == this.categoria){
        console.log(element);
        console.log(element.tatami, this.tataAka);
        
        if(element.tatami == this.tataAka){
          console.log(element.equipo);
          if(element.equipo == 'EQUIPO AKA'){
            if(element.ronda == 'ronda1'){
              this.ronda1Aka.push(element)
            }else if(element.ronda == 'ronda2'){
              this.ronda2Aka.push(element)
            }else if(element.ronda == 'ronda3'){
              this.ronda3Aka.push(element)
            }
          }
        }
      }
    });
    console.log(this.ronda1Aka,this.ronda2Aka,this.ronda3Aka);
    
    res.forEach(element => {
      if(element.idAtleta.valores[0].categoria == this.categoria){
        if(element.tatami == this.tataAo){
          if(element.equipo == 'EQUIPO AO'){
            if(element.ronda == 'ronda1'){
              this.ronda1Ao.push(element)
            }else if(element.ronda == 'ronda2'){
              this.ronda2Ao.push(element)
            }else if(element.ronda == 'ronda3'){
              this.ronda3Ao.push(element)
            }
          }
        }
      }
    });

    //////////////////ronda2///////////////////////
    this.ronda1Aka.sort(function(a,b){
      if(a.puntajeTotal < b.puntajeTotal){
        return 1;
      }
      if (a.puntajeTotal > b.puntajeTotal) {
        return -1;
      }
      return 0;
    })
    this.ronda1Ao.sort(function(a,b){
      if(a.puntajeTotal < b.puntajeTotal){
        return 1;
      }
      if (a.puntajeTotal > b.puntajeTotal) {
        return -1;
      }
      return 0;
    })
    if(this.ronda1Aka.length != 0){

      var indexAka = this.ronda1Aka.length;
      
      this.resultadosRonda1Rojos = this.ronda1Aka.slice(indexAka/2,indexAka);
      this.resultadosRonda1Rojos2 = this.ronda1Aka.slice(0,indexAka/2)

    }
    if(this.ronda1Ao.length != 0){
 
      var indexAo = this.ronda1Ao.length;
    
      this.resultadosRonda1Azules = this.ronda1Ao.slice(indexAo/2,indexAo);
      this.resultadosRonda1Azules2 = this.ronda1Ao.slice(0,indexAo/2);
      
    }


//////////////////ronda3///////////////////////
  this.ronda2Aka.sort(function(a,b){
    if(a.puntajeTotal < b.puntajeTotal){
      return 1;
    }
    if (a.puntajeTotal > b.puntajeTotal) {
      return -1;
    }
    return 0;
  })
  this.ronda2Ao.sort(function(a,b){
    if(a.puntajeTotal < b.puntajeTotal){
      return 1;
    }
    if (a.puntajeTotal > b.puntajeTotal) {
      return -1;
    }
    return 0;
  })
  if(this.ronda2Aka.length != 0){
    this.resultadosRonda2Rojos = this.ronda2Aka.splice(0,8);     
  }
  if(this.ronda2Ao.length != 0){
    this.resultadosRonda2Azules = this.ronda2Ao.splice(0,8);
  }




    //////////////////ronda4///////////////////////
    this.ronda3Aka.sort(function(a,b){
      if(a.puntajeTotal < b.puntajeTotal){
        return 1;
      }
      if (a.puntajeTotal > b.puntajeTotal) {
        return -1;
      }
      return 0;
    })
    this.ronda3Ao.sort(function(a,b){
      if(a.puntajeTotal < b.puntajeTotal){
        return 1;
      }
      if (a.puntajeTotal > b.puntajeTotal) {
        return -1;
      }
      return 0;
    })
    if(this.ronda3Aka.length != 0){
      this.resultadosRonda3Rojos = this.ronda3Aka.splice(0,3);     
    }
    if(this.ronda3Ao.length != 0){
      this.resultadosRonda3Azules = this.ronda3Ao.splice(0,3);
    }
    this.res = status

    // this.resultadosRonda1Azules = this.ronda1Ao.splice(0,8);

  } //// ontener result


  equipoaka(event,ronda){
    this._router.navigate(['/dashboard/competidor/aka/',event,this.selectDevice,this.tataAka,ronda,this.rango,this.division])
  }

  equipoao(event,ronda){
    this._router.navigate(['/dashboard/competidor/ao/',event,this.selectDevice,this.tataAo,ronda,this.rango , this.division])
  }

  semiUno(){
    localStorage.setItem('semiUnoRojo',JSON.stringify(this.resultadosRonda3Rojos[1]));
    localStorage.setItem('semiUnoAzul',JSON.stringify(this.resultadosRonda3Azules[2]));
    this._router.navigate(['/dashboard/semifinal/uno']);
  }
  semiDos(){
    localStorage.setItem('semiDosRojo',JSON.stringify(this.resultadosRonda3Rojos[2]));
    localStorage.setItem('semiDosAzul',JSON.stringify(this.resultadosRonda3Azules[1]));
    this._router.navigate(['/dashboard/semifinal/dos']);
  }
  final(){
    localStorage.setItem('finalRojo',JSON.stringify(this.resultadosRonda3Rojos[0]));
    localStorage.setItem('finalAzul',JSON.stringify(this.resultadosRonda3Azules[0]));
    this._router.navigate(['/dashboard/final/', this.division,this.rango]);
  }

}//// fin exports
