import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria96Component } from './categoria96.component';

describe('Categoria96Component', () => {
  let component: Categoria96Component;
  let fixture: ComponentFixture<Categoria96Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria96Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria96Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
