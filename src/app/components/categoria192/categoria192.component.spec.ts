import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria192Component } from './categoria192.component';

describe('Categoria192Component', () => {
  let component: Categoria192Component;
  let fixture: ComponentFixture<Categoria192Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria192Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria192Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
