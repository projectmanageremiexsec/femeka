import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria4Component } from './categoria4.component';

describe('Categoria4Component', () => {
  let component: Categoria4Component;
  let fixture: ComponentFixture<Categoria4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
