import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatosService } from '../../services/datos/datos.service';
import AtletasCompe from '../../atletas/nuevoAtleta'

@Component({
  selector: 'app-categoria4',
  templateUrl: './categoria4.component.html',
  styleUrls: ['./categoria4.component.scss']
})
export class Categoria4Component implements OnInit {

  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  @ViewChild('modalContent2', { static: true }) modalContent2?: TemplateRef<any>;
  public categoria = '';
  public genero = '';
  public rango = '';
  public division = '';
  public botontatami = true;
  public atletas = [];
  public teamRojo = [];
  public teamAzul = [];
  public kata = [];
  public selectKata='';
  public tataAka = '';
  public tataAo = '';
  public tatami = [];
  public resultados = [];
  public resultadosRojos = [];
  public resultadosAzules = [];
  public res=false;
  public numeroAtletas = 0;

  constructor(public _route:ActivatedRoute,
              public _router: Router,
              private modal: NgbModal,
              public _datos:DatosService) { }

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero =  this._route.snapshot.paramMap.get('genero')
    this.rango = this._route.snapshot.paramMap.get('rango')
    this.division =  this._route.snapshot.paramMap.get('division')
    this.katas();
    this.obtenerAtletas();
    this.obtenerResultados();
  }

  obtenerAtletas(){
    if(localStorage.getItem('registros') != null){
      this.teamRojo = JSON.parse(localStorage.getItem('registros')).atletasAka;
      this.teamAzul = JSON.parse(localStorage.getItem('registros')).atletasAo;
    }
  }

  katas(){
    this.kata = this._datos.katas
  }

  agregarAtleta(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAka(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  agregarAtletaAo(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAo(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  obtenerResultados(){
    if(localStorage.getItem('competidores') != null){
      this.resultadosRojos = JSON.parse(localStorage.getItem('competidores')).competidoresAKA;
      this.resultadosRojos.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
      this.resultadosAzules = JSON.parse(localStorage.getItem('competidores')).competidoresAO;
      this.resultadosAzules.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }

  async abrirModal(){
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  async abrirModal2(){
    this.modal.open(this.modalContent2, { size: 'lg' });
  }

  equipoaka(event){
    localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/aka/',this.selectKata,'1',this.rango,this.division])
  }

  equipoao(event){
    localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/ao/',this.selectKata,'1',this.rango,this.division])
  }

  final(){
    this._router.navigate(['/dashboard/final']);
  }
}
