import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import AtletasCompe from '../../atletas/nuevoAtleta'
import { DatosService } from '../../services/datos/datos.service';

@Component({
  selector: 'app-categoria2',
  templateUrl: './categoria2.component.html',
  styleUrls: ['./categoria2.component.scss']
})
export class Categoria2Component implements OnInit {



@ViewChild('modalContentCriterios', { static: true }) modalContentDesempate?: TemplateRef<any>;
  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  public categoria = '';
  public genero = '';
  public rango = '';
  public division = '';
  public atletas = [];
  public teamRojo = [];
  public teamAzul = [];
  public kata = [];
  public selectKata='';
  public tataAka = '';
  public tataAo = '';
  public tatami = [];
  public resultados = [];
  public resultadosRojos = [];
  public resultadosAzules = [];
  public resultados1 = [];
  public resultados2 = [];
  public res=false;
  public numeroAtletas = 0;
  public semi1 = true;
  public semi2 = true;

  constructor(public _route:ActivatedRoute,
              public _router: Router,
              private modal: NgbModal,
              public _datos:DatosService) { }

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero =  this._route.snapshot.paramMap.get('genero')
    this.rango =  this._route.snapshot.paramMap.get('rango')
    this.division = this._route.snapshot.paramMap.get('division')
    this.obtenerAtletas();
    this.katas();
    this.obtenerResultados();
  }

  obtenerAtletas(){
    if(localStorage.getItem('registros') != null){
      this.teamRojo = JSON.parse(localStorage.getItem('registros')).atletasAka;
      this.teamAzul = JSON.parse(localStorage.getItem('registros')).atletasAo;
    }
  }

  agregarAtleta(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAka(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  async abrirModal(){
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  async abrirModalDesempate(){
    this.modal.open(this.modalContentDesempate, { size: 'lg' });
  }
  equipoaka(event){
    localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/aka/',this.selectKata,'1',this.rango,this.division])
  }

  katas(){
    this.kata = this._datos.katas
  }

  obtenerResultados(){
    if(localStorage.getItem('competidores') != null){
      this.resultadosRojos = JSON.parse(localStorage.getItem('competidores')).competidoresAKA;
      this.resultadosRojos.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }

  final(){
    localStorage.setItem('finalRojo',JSON.stringify(this.teamRojo[0]));
    localStorage.setItem('finalAzul',JSON.stringify(this.teamRojo[1]));
    this._router.navigate(['/dashboard/final']);
  }

}
