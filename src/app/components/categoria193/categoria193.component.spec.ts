import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria193Component } from './categoria193.component';

describe('Categoria193Component', () => {
  let component: Categoria193Component;
  let fixture: ComponentFixture<Categoria193Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria193Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria193Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
