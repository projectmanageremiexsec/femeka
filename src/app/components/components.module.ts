import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RankComponent } from './rank/rank.component';
import { Categoria10Component } from './categoria10/categoria10.component';
import { Categoria24Component } from './categoria24/categoria24.component';
import { Categoria48Component } from './categoria48/categoria48.component';
import { Categoria96Component } from './categoria96/categoria96.component';
import { Categoria192Component } from './categoria192/categoria192.component';
import { Categoria193Component } from './categoria193/categoria193.component';
import { Categoria2Component } from './categoria2/categoria2.component';
import { Categoria4Component } from './categoria4/categoria4.component';

@NgModule({
  declarations: [
    RankComponent,
    Categoria10Component,
    Categoria24Component,
    Categoria48Component,
    Categoria96Component,
    Categoria192Component,
    Categoria193Component,
    Categoria2Component,
    Categoria4Component
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
  ],
  exports:[
    RankComponent,
    Categoria2Component,
    Categoria4Component,
    Categoria10Component,
    Categoria24Component,
    Categoria48Component,
    Categoria96Component,
    Categoria192Component,
    Categoria193Component
  ],
  schemas: [ 
    CUSTOM_ELEMENTS_SCHEMA
   ],
})
export class ComponentsModule { }
