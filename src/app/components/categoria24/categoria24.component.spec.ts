import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria24Component } from './categoria24.component';

describe('Categoria24Component', () => {
  let component: Categoria24Component;
  let fixture: ComponentFixture<Categoria24Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria24Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria24Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
