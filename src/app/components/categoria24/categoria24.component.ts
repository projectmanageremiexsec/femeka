import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatosService } from '../../services/datos/datos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import AtletasCompe from '../../atletas/nuevoAtleta'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-categoria24',
  templateUrl: './categoria24.component.html',
  styleUrls: ['./categoria24.component.scss']
})
export class Categoria24Component implements OnInit {

  @ViewChild('modalContent', { static: true }) modalContent?: TemplateRef<any>;
  @ViewChild('modalContent2', { static: true }) modalContent2?: TemplateRef<any>;
  @ViewChild('modalContentCriterios', { static: true }) modalContentDesempate?: TemplateRef<any>;
  @Input() categoria = '';
  @Input() genero = '';
  @Input() rango = '';
  @Input() division = '';
  public botontatami = true;
  public atletas = [];
  public teamRojo = [];
  public teamAzul = [];
  public kata = [];
  public selectKata='';
  public tataAka = '';
  public tataAo = '';
  public tatami = [];
  public resultados = [];
  public resultadosRojos = [];
  public resultadosAzules = [];
  public ronda1Aka = [];
  public ronda1Ao = [];
  public ronda2Aka = [];
  public ronda2Ao = [];
  public res=false;
  public numeroAtletas = 0;
  public semi1 = true;
  public semi2 = true;
  public ronda1 = 'ronda1';
  public ronda2 = 'ronda2';

  constructor(public _route:ActivatedRoute,
              public _router: Router,
              public _datos:DatosService,
              private modal: NgbModal) { }

  ngOnInit(): void {
    this.categoria = this._route.snapshot.paramMap.get('categoria')
    this.genero =  this._route.snapshot.paramMap.get('genero')
    this.rango = this._route.snapshot.paramMap.get('rango')
    this.division =  this._route.snapshot.paramMap.get('division')
    this.katas();
    this.obtenerAtletas();
    this.obtenerResultados();
  }

  obtenerAtletas(){
    if(localStorage.getItem('registros') != null){
      this.teamRojo = JSON.parse(localStorage.getItem('registros')).atletasAka;
      this.teamAzul = JSON.parse(localStorage.getItem('registros')).atletasAo;
    }
  }

  async abrirModalDesempate(){
    this.modal.open(this.modalContentDesempate, { size: 'lg' });
  }

  agregarAtleta(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAka(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  agregarAtletaAo(form:NgForm){
    let data = Object.assign(form.value,{categoria:this.categoria});
    let compe = new AtletasCompe();
    compe.agregarAtletaEquipoAo(data);
    this.obtenerAtletas();
    this.modal.dismissAll();
  }

  katas(){
    this.kata = this._datos.katas
  }

  async abrirModal(){
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  async abrirModal2(){
    this.modal.open(this.modalContent2, { size: 'lg' });
  }

  obtenerResultados(){
    if(localStorage.getItem('competidores') != null){
      this.resultadosRojos = JSON.parse(localStorage.getItem('competidores')).competidoresAKA;
      this.resultadosRojos.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
      this.resultadosAzules = JSON.parse(localStorage.getItem('competidores')).competidoresAO;
      this.resultadosAzules.sort(function(a,b){
        if(a.calificacionFinal < b.calificacionFinal){
          return 1;
        }
        if (a.calificacionFinal > b.calificacionFinal) {
          return -1;
        }
        return 0;
      })
    }
  }

  equipoaka(event){
    localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/aka/',this.selectKata,'1',this.rango,this.division])
  }

  equipoao(event,ronda){
    localStorage.setItem('atleta', JSON.stringify(event))
    this._router.navigate(['/dashboard/competidor/ao/',this.selectKata,'1',this.rango,this.division])
  }

  semiUno(){
    /* localStorage.setItem('semiUnoRojo',JSON.stringify(this.resultadosRonda2Rojos[1]));
    localStorage.setItem('semiUnoAzul',JSON.stringify(this.resultadosRonda2Azules[2]));
    this._router.navigate(['/dashboard/semifinal/uno']); */
  }
  semiDos(){
    /* localStorage.setItem('semiDosRojo',JSON.stringify(this.resultadosRonda2Rojos[2]));
    localStorage.setItem('semiDosAzul',JSON.stringify(this.resultadosRonda2Azules[1]));
    this._router.navigate(['/dashboard/semifinal/dos']); */
  }
  final(){
    /* localStorage.setItem('finalRojo',JSON.stringify(this.resultadosRonda2Rojos[0]));
    localStorage.setItem('finalAzul',JSON.stringify(this.resultadosRonda2Azules[0]));
    this._router.navigate(['/dashboard/final']); */
  }
}
