import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Categoria48Component } from './categoria48.component';

describe('Categoria48Component', () => {
  let component: Categoria48Component;
  let fixture: ComponentFixture<Categoria48Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Categoria48Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Categoria48Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
