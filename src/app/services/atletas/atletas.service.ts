import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { URL } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class AtletasService {
  public url;
  constructor(private _http:HttpClient) {
    this.url = URL;
  }

  obtenerAtletas(){
    let uri = this.url+'/obetner/atletas';
    return this._http.get(uri);
  }

  obtenerAtletasOrdenados(){
    let uri = this.url+'/distic/estado';
    return this._http.get(uri);
  }

  obtenerAtleta(id){
    let uri = this.url+'/obtener/idarreglo/'+id;
    return this._http.get(uri);
  }

  actulizarKata(id, body){
    let uri = this.url+'/actualiza/kata/realizada/'+id;
    return this._http.put(uri, body);
  }

  asignarPuntaje(body){
    let uri = this.url+'/enviar/tabla/Resultados';
    return this._http.post(uri, body);
  }

  obtenerResultados(){
    let uri = this.url+'/obtener/resultados';
    return this._http.get(uri);
  }

  obtenerKatas(){
    let uri = this.url+'/obtener/katas';
    return this._http.get(uri);
  }

  obtenerCategorias(){
    let uri = this.url+'/obtener/categorias';
    return this._http.get(uri);
  }

  obtenerCategoriasKyu(){
    let uri = this.url+'/obtener/grados/kyu';
    return this._http.get(uri);
  }

  obtenerTatami(){
    let uri = this.url+'/obtener/tatamis';
    return this._http.get(uri);
  }

  obtenerDivisiones(){
    let uri = this.url+'/obtener/divisiones';
    return this._http.get(uri);
  }

  enviarResultadosSemiUno(body:any){
    let uri = this.url+'/enviar/tabla/Primer/semi';
    return this._http.post(uri,body);
  }
  obtenerResultadosSemiUno(){
    let uri = this.url+'/obtener/primeros/semi/finalistas';
    return this._http.get(uri);
  }
  enviarResultadosSemiDos(body:any){
    let uri = this.url+'/enviar/tabla/Segunda/semi';
    return this._http.post(uri,body);
  }
  obtenerResultadosSemiDos(){
    let uri = this.url+'/obtener/segundos/semi/finalistas';
    return this._http.get(uri);
  }

  enviarResultadosFinal(body:any){
    let uri = this.url+'/enviar/tabla/Finalistas';
    return this._http.post(uri,body);
  }
  obtenerResultadosFinal(){
    let uri = this.url+'/obtener/finalistas';
    return this._http.get(uri);
  }


  // enviar tabla winners y obtenerla

  enviarTablaWinner(body:any){
    let uri = this.url+'/enviar/tabla/winners';
    return this._http.post(uri,body);
  }
  obtenerTablaWinner(){
    let uri = this.url+'/obtener/ganadores';
    return this._http.get(uri);
  }


  // enviar tabla categoria terminada
  enviarTablaCategoriaTerminada(body:any){
    let uri = this.url+'/enviar/categoria/terminada';
    return this._http.post(uri,body);


  }

  obtenerCategoriasNegrasKumite(){
    let uri = this.url+'/obtener/categorias/kumite/negra';
    return this._http.get(uri);
  }

  obtenerTablaCategoriaTerminada(){
    let uri = this.url+'/obtener/categorias/terminadas';
    return this._http.get(uri);
  }

  agregarCompetencia(body){
    let uri = this.url+'/registro/competencias/cate';
    return this._http.post(uri,body);
  }

  actualizarCompetencia(id, body){
    let uri = this.url+'/actualizar/competencias/'+id;
    return this._http.put(uri,body);
  }

  obtenerCompetencias(body){
    let uri = this.url+'/obtener/comptenecias/cate';
    return this._http.post(uri,body);
  }

}




