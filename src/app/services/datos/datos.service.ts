import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatosService {

  divisiones: any[] = [
    {
      division:"10°, 9°, Kyu Cintas Blancas"
    },
    {
      division:"3°, 2° y 1°, Kyu Cintas Cafés, o Equivalentes"
    },
    {
      division:"8°, 7°, Kyu Cintas Amarillas, Naranjas"
    },
    {
      division:"6°, 5° y 4°, Kyu Cintas Verdes"
    }
  ];

  hombresKyu:any[] = [
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Categoria Mayor + 18 años"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Infantil B (2012)"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ];

  hombresNegras:any [] = [
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002) Equipo"
    },
    {
      categoria:"Categoria Mayor +18 Individual"
    },
    {
      categoria:"Juvenil Mayor (2006-2005) Equipo"
    },
    {
      categoria:"Categoria Mayor +18 Equipo"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Infantil B (2012)"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ];

  mujeresKyu:any []=[
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Infantil B (2012)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Categoria Mayor + 18 años"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ];

  /// kyu categorias mixtas

  cintasKyu:any []=[
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Infantil B (2012)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Categoria Mayor + 18 años"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Categoria Mayor + 18 años"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Infantil B (2012)"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ];
  ///////////////////////////////////////////


  ///// cintas negras mixtas

  cintasNegras:any [] = [
    {
      categoria:"Infantil B (2012)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Categoria Mayor +18 Individual"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002) Equipo"
    },
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005) Equipo"
    },
    {
      categoria:"Categoria Mayor +18 Equipo"
    },
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002) Equipo"
    },
    {
      categoria:"Categoria Mayor +18 Individual"
    },
    {
      categoria:"Juvenil Mayor (2006-2005) Equipo"
    },
    {
      categoria:"Categoria Mayor +18 Equipo"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Infantil B (2012)"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ];


  /////////////////////////////////////

  mujeresNegras:any [] = [
    {
      categoria:"Infantil B (2012)"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002)"
    },
    {
      categoria:"Infantil C (2013)"
    },
    {
      categoria:"Infantil Mayor (2010-2009)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005)"
    },
    {
      categoria:"Categoria Mayor +18 Individual"
    },
    {
      categoria:"Juvenil Superior (2004-2003-2002) Equipo"
    },
    {
      categoria:"Infantil A (2011)"
    },
    {
      categoria:"Juvenil Menor (2008-2007)"
    },
    {
      categoria:"Juvenil Mayor (2006-2005) Equipo"
    },
    {
      categoria:"Categoria Mayor +18 Equipo"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ];

  parakarate:any [] = [
    {
      categoria:"Principiantes parálisis cerebral"
    },
    {
      categoria:"Avanzados Especiales (sindrome de Down)"
    },
    {
      categoria:"Avanzados Sillas de rueda y/o amputaciones"
    },
    {
      categoria:"Intermedios parálisis cerebral"
    },
    {
      categoria:"Principiantes Especiales (sindrome de Down)"
    },
    {
      categoria:"Intermedios Especiales (sindrome de Down)"
    },
    {
      categoria:"Principiantes Sillas de rueda y/o amputaciones"
    },
    {
      categoria:"Avanzados parálisis cerebral"
    },
    {
      categoria:"Intermedios Sordos"
    },
    {
      categoria:"Avanzados Sordos"
    },
    {
      categoria:"Intermedios Sillas de rueda y/o amputaciones"
    },
    {
      categoria:"Principiantes Ciegos y débiles visuales"
    },
    {
      categoria:"Intermedios Ciegos y débiles visuales"
    },
    {
      categoria:"Avanzados Ciegos y débiles visuales"
    },
    {
      categoria:"Principiantes Sordos"
    }, {
      categoria:"OPEN"
    }, {
      categoria:"PRUEBAS"
    }, {
      categoria:"FINALES"
    }
  ]

  katas: any[] = [
    {
      numero: "1",
      nombre: "Anan"
    },
    {
    numero: "2",
    nombre: "Anan Dai"
    },{
    numero: "6",
    nombre: "Bassai Dai"
  },{
    numero: "7",
    nombre: "Bassai Sho"
  },{
    numero: "8",
    nombre: "Chatanyara Kusanku"
  },{
    numero: "9",
    nombre: "Chibana No Kushanku"
  },{
    numero: "10",
    nombre: "Chinte"
  },{
    numero: "11",
    nombre: "Chinto"
  },{
    numero: "12",
    nombre: "Enpi"
  },{
    numero: "15",
    nombre: "Gankaku"
  },{
    numero: "17",
    nombre: "Gakisai (Geksai) 1"
  },{
    numero: "18",
    nombre: "Gakisai (Geksai) 2"
  },{
    numero: "19",
    nombre: "Gojushiho "
  },{
    numero: "20",
    nombre: "Gojushiho Dai"
  },{
    numero: "21",
    nombre: "Gojushiho sho"
  },{
    numero: "22",
    nombre: "Hakucho"
  },{
    numero: "23",
    nombre: "Hangetsu"
  },{
    numero: "24",
    nombre: "Haifa (Haffa)"
  },{
    numero: "25",
    nombre: "Heian Shodan"
  },{
    numero: "26",
    nombre: "Heian Nidan"
  },{
    numero: "27",
    nombre: "Heian Sandan"
  },{
    numero: "28",
    nombre: "Heian Yondan"
  },{
    numero: "29",
    nombre: "Heian Godan"
  },{
    numero: "30",
    nombre: "Heiku"
  },{
    numero: "31",
    nombre: "Ishimine Bassai"
  },{
    numero: "32",
    nombre: "Itosu Rohai Shodan"
  },{
    numero: "33",
    nombre: "Itosu Rohai Nidan"
  },{
    numero: "34",
    nombre: "Itosu Rohai Sandan"
  },{
    numero: "35",
    nombre: "Jiin"
  },{
    numero: "36",
    nombre: "Jion"
  },{
    numero: "37",
    nombre: "Jitte"
  },{
    numero: "40",
    nombre: "Kanku Dai"
  },{
    numero: "41",
    nombre: "Kanku Sho"
  },{
    numero: "42",
    nombre: "Kanshu"
  },{
    numero: "43",
    nombre: "Kishimoto No Kushanku"
  },{
    numero: "44",
    nombre: "Kousoukun"
  },{
    numero: "45",
    nombre: "Kousoukun Dai"
  },{
    numero: "46",
    nombre: "Kousoukun Sho"
  },{
    numero: "47",
    nombre: "Kururunfa"
  },{
    numero: "52",
    nombre: "Matsumura Bassai"
  },{
    numero: "53",
    nombre: "Matsumura Rohai"
  },{
    numero: "54",
    nombre: "Meiko"
  },{
    numero: "56",
    nombre: "Naifanchin Shodan"
  },{
    numero: "57",
    nombre: "Naifanchin Nidan"
  },{
    numero: "58",
    nombre: "Naifanchin Sandan"
  },{
    numero: "59",
    nombre: "Naihanchi"
  },{
    numero: "60",
    nombre: "Nijushiho"
  },{
    numero: "61",
    nombre: "Nipaipo"
  },{
    numero: "62",
    nombre: "Niseishi"
  },{
    numero: "63",
    nombre: "Ohan"
  },{
    numero: "64",
    nombre: "Ohan Dai"
  },{
    numero: "66",
    nombre: "Pachu"
  },{
    numero: "67",
    nombre: "Paiku"
  },{
    numero: "68",
    nombre: "Papuren"
  },{
    numero: "70",
    nombre: "Pinan Shodan"
  },{
    numero: "71",
    nombre: "Pinan Nidan"
  },{
    numero: "72",
    nombre: "Pinan Sandan"
  },{
    numero: "73",
    nombre: "Pinan Yondan"
  },{
    numero: "74",
    nombre: "Pinan Godan"
  },{
    numero: "75",
    nombre: "Rohai"
  },{
    numero: "76",
    nombre: "Saifa"
  },{
    numero: "77",
    nombre: "Sanchin"
  },{
    numero: "78",
    nombre: "Sansai"
  },{
    numero: "79",
    nombre: "Sanseiru"
  },{
    numero: "80",
    nombre: "Sanseru"
  },{
    numero: "82",
    nombre: "Seienchin (Saiyunchin)"
  },{
    numero: "83",
    nombre: "Seipai"
  },{
    numero: "85",
    nombre: "Seishan"
  },{
    numero: "86",
    nombre: "Seisan (Sesan)"
  },{
    numero: "87",
    nombre: "Shiho Kousoukun"
  },{
    numero: "90",
    nombre: "Shisochin"
  },{
    numero: "91",
    nombre: "Sochin"
  },{
    numero: "92",
    nombre: "Suparimpei"
  },{
    numero: "93",
    nombre: "Tekki Shodan"
  },{
    numero: "94",
    nombre: "Tekki Nidan"
  },{
    numero: "95",
    nombre: "Tekki Sandan"
  },{
    numero: "97",
    nombre: "Tomari Bassai"
  },{
    numero: "98",
    nombre: "Unshu"
  },{
    numero: "99",
    nombre: "Unsu"
  },{
    numero: "100",
    nombre: "Useishi"
  },{
    numero: "101",
    nombre: "Wankan"
  },{
    numero: "102",
    nombre: "Wanshu"
  }]

  constructor() { }
}
