import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      titulo: 'Dashboard',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'TABLAS', url: 'categoria/rango' },
        /* { titulo: 'KUMITE', url: 'registro/kumite' },
        { titulo: 'DOCUMENTOS', url: 'documentos' },
        { titulo: 'GRAFICAS KUMITE', url: 'graficas/kumite' }, */
        // { titulo: 'DOCUMENTOS', url: 'documentos' },
        /* { titulo: 'REGISTRO KATA', url: 'documentos' }, */
        //  { titulo: 'DOCUMENTOS', url: 'documentos' },
        // { titulo: 'GRAFICAS KUMITE', url: 'graficas/kumite' },
        /* { titulo: 'LOGOUT', url: '/login' }, */

      ]
    },
  ];

  constructor() { }
}
